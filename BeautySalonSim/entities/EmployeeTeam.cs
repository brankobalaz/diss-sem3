﻿using OSPABA;
using simulation;

namespace entities;

public class EmployeeTeam
{
    private List<Employee> _employeesForSort = new();
    public List<Employee> Employees { get; } = new();
    public Simulation MySim;

    public EmployeeTeam(int employeeCount, Simulation mySim)
    {
        MySim = mySim;

        for (var i = 0; i < employeeCount; i++)
        {
            var newEmployee = new Employee(mySim);
            _employeesForSort.Add(newEmployee);
            Employees.Add(newEmployee);
        }
    }

    public double Utilization => _employeesForSort.Average(x => x.Utilization);

    public Employee? GetNotBusyEmployee()
    {
        _employeesForSort.Sort();
        return _employeesForSort[0].IsBusy ? null : _employeesForSort[0];
    }
}