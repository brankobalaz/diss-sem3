﻿using System.Security.AccessControl;
using OSPABA;
using simulation;
using utils;

namespace entities;

public class Customer : Entity
{
    public ProcedureState WashingProcedure { get; set; } = ProcedureState.NotRequested;
    public ProcedureState HairProcedure { get; set; } = ProcedureState.NotRequested;
    public ProcedureState FaceCleansingProcedure { get; set; } = ProcedureState.NotRequested;
    public ProcedureState MakeupProcedure { get; set; } = ProcedureState.NotRequested;
    public ProcedureState CheckInProcedure { get; set; } = ProcedureState.NotRequested;
    public ProcedureState PayProcedure { get; set; } = ProcedureState.NotRequested;

    public ProcedureState Parking { get; set; } = ProcedureState.NotRequested;
    public ProcedureState WalkToSalon { get; set; } = ProcedureState.NotRequested;
    public ProcedureState WalkToCar { get; set; } = ProcedureState.NotRequested;
    public ProcedureState LeavingByCar { get; set; } = ProcedureState.NotRequested;

    public bool IsLeavingWithoutServeInSalon { get; set; } = false;

    public double EnterSystemTimestamp { get; set; }
    public double EnterSystemClockTimestamp => EnterSystemTimestamp + Config.SalonOpenClockSeconds;

    public double EnterSalonTimestamp { get; set; }
    public double EnterSalonClockTimestamp => EnterSalonTimestamp + Config.SalonOpenClockSeconds;

    public double EnterCheckInQueueTimestamp { get; set; }
    public double EnterHairQueueTimestamp { get; set; }
    public double EnterFaceQueueTimestamp { get; set; }
    public double EnterPayQueueTimestamp { get; set; }

    public Car? Car { get; set; }

    public Customer(Simulation mySim) : base(mySim)
    {
        EnterSystemTimestamp = MySim.CurrentTime;
    }

    public Customer(int id, Simulation mySim) : base(id, mySim)
    {
    }
}