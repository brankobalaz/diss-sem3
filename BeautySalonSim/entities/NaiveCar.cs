﻿using System.Security.AccessControl;
using BeautySalonSim.utils;
using OSPABA;
using simulation;
using utils;

namespace entities;

public class NaiveCar : Car
{
    public NaiveCar(Simulation mySim) : base(mySim)
    {
    }

    public NaiveCar(int id, Simulation mySim) : base(id, mySim)
    {
    }

    public override string StrategyName => "Naive";

    protected override void NavigateSpawn(ParkingLot lot)
    {
        // Go home
    }

    protected override void NavigateInParkingLotRow(ParkingLot lot)
    {
        var isNextPlaceTaken = CurrentPos.IsAtEndOfRow || lot.IsPositionTaken(CurrentPos.NextInRowPos);
        var isCurrentPlaceTaken = lot.IsPositionTaken(CurrentPos);

        if (isNextPlaceTaken && !isCurrentPlaceTaken)
            lot.SeizeCurrPos(this);
        else
            MoveToNextPlace();
    }

    protected override void NavigateCrossroads(ParkingLot lot)
    {
        if (lot.IsPositionTaken(CurrentPos.NextInRowPos))
            MoveToNextCrossRoad();
        else
            MoveToNextPlace();
    }

    protected void MoveToNextCrossRoad()
    {
        if (CurrentPos.Row == Config.ParkingLotRowsCount)
        {
            MoveToNextPlace();
        }
        else
        {
            DestinationPos.Col = 0;
            DestinationPos.Row++;
        }
    }

    protected void MoveToNextPlace()
    {
        DestinationPos.Col++;
        if (DestinationPos.Col == Config.ParkingLotPlacesInRowCount + 1)
        {
            if (CurrentPos.Row == Config.ParkingLotRowsCount)
            {
                SetParkingUnsuccessful();
            }
            else
            {
                MoveToNextCrossRoad();
                SatisfactionPenaltyCounter++;
            }
        }
    }
}