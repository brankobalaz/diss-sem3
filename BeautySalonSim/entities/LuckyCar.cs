﻿using BeautySalonSim.utils;
using OSPABA;
using OSPRNG;
using simulation;
using utils;

namespace entities;

public class LuckyCar : Car
{
    public List<int> SeenRows { get; set; } = new();

    public Pos? BestSeenFreePosition { get; set; }

    public RNG<double> _crossRoadRng { get; set; }
    public RNG<double> _inRowRng { get; set; }

    public LuckyCar(Simulation mySim) : base(mySim)
    {
        _crossRoadRng = new UniformContinuousRNG(0, 1);
        _inRowRng = new UniformContinuousRNG(0, 1);
    }

    public LuckyCar(int id, Simulation mySim) : base(id, mySim)
    {
    }

    public override string StrategyName => "Lucky";

    protected override void NavigateCrossroads(ParkingLot lot)
    {
        for (var i = 1; i <= 5; i++)
            if (!lot[CurrentPos.Row, i].IsTaken &&
                (BestSeenFreePosition == null || // Nemám nič, alebo mám lepšiu
                 CurrentPos.Row <= BestSeenFreePosition.Row && i > BestSeenFreePosition.Col))
                BestSeenFreePosition = new Pos(CurrentPos.Row, i);

        if (CurrentPos.Row == Config.ParkingLotRowsCount)
            // Som pred posledným radom - vojdem vždy
            MoveIntoCurrentRow();
        else if (BestSeenFreePosition != null && BestSeenFreePosition.Row == CurrentPos.Row)
            // Ak vidím voľné v tomto rade, idem do radu
            MoveIntoCurrentRow();
        else
            // Som v rade A/B, a nie je tam voľné v prvých 5
            MoveToNextPlaceOrCrossroad();
    }

    protected void MoveToNextPlaceOrCrossroad()
    {
        // V rade nie je prvých 5 voľných
        if (SatisfactionPenaltyCounter == 0)
        {
            // Prvý prejazd - hodí si mincou, či do radu vojde
            var rand = _crossRoadRng.Sample();
            var rowBias = Config.RowBias;

            if (rand < rowBias)
                MoveIntoCurrentRow();
            else
                DestinationPos.Row++;
        }
        else
        {
            // Nie prvý prejaz a nie je prvých 5 voľných - choď tam, kde si ešte nebol

            if (GetFirstUnseenRow() == CurrentPos.Row)
                // Tento riadok ešte nevidel
                MoveIntoCurrentRow();
            else
                // Nie prvý prechod, ale tento riadok už videl
                DestinationPos.Row++;
        }
    }

    protected override void NavigateInParkingLotRow(ParkingLot lot)
    {
        var isNextPlaceTaken = CurrentPos.IsAtEndOfRow || lot.IsPositionTaken(CurrentPos.NextInRowPos);
        var isCurrentPlaceTaken = lot.IsPositionTaken(CurrentPos);

        // Aktualizuj, iba ak je ďalšia pozícia lepšia
        if (!isNextPlaceTaken &&
            (BestSeenFreePosition == null || CurrentPos.NextInRowPos.Col > BestSeenFreePosition.Col))
            BestSeenFreePosition = CurrentPos.NextInRowPos;

        if (isNextPlaceTaken && !isCurrentPlaceTaken &&
            BestSeenFreePosition != null &&
            BestSeenFreePosition.Equals(CurrentPos.NextInRowPos))
            // Ďalšia pozícia je moja najlepšia, ale je obsadená a moja pozícia je voľná
            BestSeenFreePosition = CurrentPos;

        if (BestSeenFreePosition != null && BestSeenFreePosition.Equals(CurrentPos))
        {
            // Som na najlepšej videnej pozícií    
            if (!isCurrentPlaceTaken)
            {
                // Pozícia je voľná
                var rand = _inRowRng.Sample();
                var colBias = (CurrentPos.Col + Config.ColBias) / (double) Config.ParkingLotPlacesInRowCount;

                if (SatisfactionPenaltyCounter != 0)
                    // Pri druhom prechode berie, čo má
                    colBias = 1;

                if (rand < colBias)
                    // Miesta na začiatku majú istú malú pravdepodobnosť, že tam nezaparkuje
                    lot.SeizeCurrPos(this);
            }
            else
            {
                BestSeenFreePosition = null;
            }
        }

        if (!IsParked) MoveToNextPlace();
    }

    private void MoveIntoCurrentRow()
    {
        MoveToNextPlace();
        SeenRows.Add(CurrentPos.Row);
    }

    private int GetFirstUnseenRow()
    {
        var unseenRow = 1;
        while (SeenRows.Contains(unseenRow)) unseenRow++;

        return unseenRow == Config.ParkingLotRowsCount + 1 ? -1 : unseenRow;
    }

    protected override void NavigateSpawn(ParkingLot lot)
    {
        // Pokiaľ si pamätám dobrú pozíciu, alebo som nevidel všetko
        if (BestSeenFreePosition != null || GetFirstUnseenRow() != -1)
            DestinationPos = Pos.CrossroadAPos;
        else
            SetParkingUnsuccessful();
    }

    protected void MoveToNextPlace()
    {
        if (CurrentPos.IsAtEndOfRow)
            MoveToSpawnWithPenalty();
        else
            DestinationPos.Col++;
    }

    protected void MoveToSpawnWithPenalty()
    {
        DestinationPos = Pos.SpawnPos;
        SatisfactionPenaltyCounter++;
    }
}