﻿using BeautySalonSim.utils;
using OSPABA;
using simulation;

namespace entities;

public class BasicCar : Car
{
    public List<int> SeenRows { get; set; } = new();

    public Pos? FarthestSeenFreePosition { get; set; }

    public BasicCar(Simulation mySim) : base(mySim)
    {
    }

    public BasicCar(int id, Simulation mySim) : base(id, mySim)
    {
    }

    public override string StrategyName => "Basic";

    protected override void NavigateCrossroads(ParkingLot lot)
    {
        FarthestSeenFreePosition = null;

        for (var i = 1; i <= 5; i++)
            if (!lot[CurrentPos.Row, i].IsTaken)
                FarthestSeenFreePosition = new Pos(CurrentPos.Row, i);

        if (CurrentPos.Row == Config.ParkingLotRowsCount)
            // Som pred posledným radom - vojdem vždy
            MoveIntoCurrentRow();
        else if (FarthestSeenFreePosition != null)
            // Ak vidím voľné, idem do radu
            MoveIntoCurrentRow();
        else
            // Som v rade A/B, a nie je tam voľné v prvých 5
            MoveToNextPlaceOrCrossroad();
    }

    protected void MoveToNextPlaceOrCrossroad()
    {
        // V rade nie je prvých 5 voľných
        if (SatisfactionPenaltyCounter == 0)
        {
            // Prvý prejazd - Na ďalšiu križovatku
            DestinationPos.Row++;
        }
        else
        {
            // Nie prvý prejaz a nie je prvých 5 voľných - choď tam, kde si ešte nebol

            if (GetFirstUnseenRow() == CurrentPos.Row)
                // Tento riadok ešte nevidel
                MoveIntoCurrentRow();
            else
                // Nie prvý prechod, ale tento riadok už videl
                DestinationPos.Row++;
        }
    }

    protected override void NavigateInParkingLotRow(ParkingLot lot)
    {
        var isNextPlaceTaken = CurrentPos.IsAtEndOfRow || lot.IsPositionTaken(CurrentPos.NextInRowPos);
        var isCurrentPlaceTaken = lot.IsPositionTaken(CurrentPos);

        if (!isNextPlaceTaken)
            FarthestSeenFreePosition = CurrentPos.NextInRowPos;

        // Ak je moje miesto voľné a predo mnou obsadené, tak zaparkujem
        if (FarthestSeenFreePosition != null && FarthestSeenFreePosition.Equals(CurrentPos) && !isCurrentPlaceTaken)
            lot.SeizeCurrPos(this);
        else
            MoveToNextPlace();
    }

    private void MoveIntoCurrentRow()
    {
        MoveToNextPlace();
        SeenRows.Add(CurrentPos.Row);
    }

    private int GetFirstUnseenRow()
    {
        var unseenRow = 1;
        while (SeenRows.Contains(unseenRow)) unseenRow++;

        return unseenRow == Config.ParkingLotRowsCount + 1 ? -1 : unseenRow;
    }

    protected override void NavigateSpawn(ParkingLot lot)
    {
        // Už videl všetko, ide domov
        if (GetFirstUnseenRow() == -1)
            SetParkingUnsuccessful();
        else
            DestinationPos = Pos.CrossroadAPos;
    }

    protected void MoveToNextPlace()
    {
        if (CurrentPos.IsAtEndOfRow)
            MoveToSpawnWithPenalty();
        else
            DestinationPos.Col++;
    }

    protected void MoveToSpawnWithPenalty()
    {
        DestinationPos = Pos.SpawnPos;
        SatisfactionPenaltyCounter++;
    }
}