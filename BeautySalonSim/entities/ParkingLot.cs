﻿using System.Collections;
using System.Runtime.Versioning;
using BeautySalonSim.utils;
using OSPABA;
using simulation;
using utils;

namespace entities;

/// <summary>
/// Indexed from 1!
/// </summary>
public class ParkingLot : Entity
{
    private readonly List<ParkingPlace> _parkingPlaces = new();

    public int Rows { get; }
    public int Cols { get; }

    public double Utilization => _parkingPlaces.Average(x => x.Utilization);

    public ParkingLot(Simulation mySim) : base(mySim)
    {
        Rows = Config.ParkingLotRowsCount;
        Cols = Config.ParkingLotPlacesInRowCount;

        for (var i = 0; i < Config.ParkingLotRowsCount * Config.ParkingLotPlacesInRowCount; i++)
            _parkingPlaces.Add(new ParkingPlace(MySim));
    }

    public ParkingLot(int id, Simulation mySim) : base(id, mySim)
    {
    }

    public bool IsPositionTaken(Pos pos)
    {
        return this[pos.Row, pos.Col].IsTaken;
    }

    public double GetUtilizationAt(Pos position)
    {
        return this[position.Row, position.Col].Utilization;
    }

    public ParkingPlace this[int row, int column]
    {
        get
        {
            row -= 1;
            column -= 1;

            if (row < 0 || Config.ParkingLotRowsCount <= row)
                throw new IndexOutOfRangeException("Zlý riadok");

            if (column < 0 || Config.ParkingLotPlacesInRowCount <= column)
                throw new IndexOutOfRangeException("Zlý stĺpec");

            var i = Config.ParkingLotPlacesInRowCount * row + column;

            return _parkingPlaces[i];
        }
    }

    public void SeizeCurrPos(Car car)
    {
        this[car.CurrentPos].Seize();
        car.IsParked = true;
        car.Customer!.Parking = ProcedureState.Done;
    }

    public void ReleaseCurrPos(Car car)
    {
        this[car.CurrentPos].Release();
        car.IsParked = false;
    }

    public ParkingPlace this[Pos pos] => this[pos.Row, pos.Col];
}