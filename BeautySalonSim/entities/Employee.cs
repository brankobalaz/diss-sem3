﻿using OSPABA;
using OSPStat;

namespace entities;

public class Employee : Entity, IComparable<Employee>
{
    public bool IsBusy { get; private set; } = false;
    public double ServedTime { get; private set; } = 0;
    private double _lastSeizeTime;

    public double Utilization => GetUtilization();
    
    public Employee(Simulation mySim) : base(mySim)
    {
    }

    public Employee(int id, Simulation mySim) : base(id, mySim)
    {
    }

    public void Release()
    {
        IsBusy = false;
        ServedTime += MySim.CurrentTime - _lastSeizeTime;
    }

    public void Seize()
    {
        IsBusy = true;
        _lastSeizeTime = MySim.CurrentTime;
    }

    private double GetUtilization()
    {
        var atTime = MySim.CurrentTime;

        if (IsBusy)
            return (ServedTime + (atTime - _lastSeizeTime)) / atTime;

        return ServedTime / atTime;
    }

    public int CompareTo(Employee? other)
    {
        return IsBusy switch
        {
            false when other!.IsBusy => -1,
            true when !other!.IsBusy => 1,
            _ => ServedTime.CompareTo(other.ServedTime)
        };
    }
}