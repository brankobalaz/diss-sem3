﻿using System.Runtime.CompilerServices;
using BeautySalonSim.utils;
using OSPABA;
using simulation;
using utils;

namespace entities;

public abstract class Car : Entity
{
    private static List<int> _forbiddenColors = new() {1, 43, 47, 94, 136, 137};

    public Pos CurrentPos { get; set; }
    public Pos DestinationPos { get; set; }

    public Customer? Customer { get; set; }
    public double EnterSystemTimestamp { get; set; }
    public double EnterSystemClockTimestamp => EnterSystemTimestamp + Config.SalonOpenClockSeconds;

    public bool IsParked { get; set; } = false;
    public bool ParkingUnsuccessful { get; set; } = false;

    public int SatisfactionPenaltyCounter { get; set; } = 0;

    public int Color { get; private set; } // 0 - 139

    public Car(Simulation mySim) : base(mySim)
    {
        EnterSystemTimestamp = mySim.CurrentTime;

        var r = new Random();
        do
        {
            Color = r.Next(140);
        } while (_forbiddenColors.Contains(Color));

        CurrentPos = new Pos(0, Config.ParkingLotPlacesInRowCount);
        DestinationPos = new Pos(-100, -100);
    }

    public Car(int id, Simulation mySim) : base(id, mySim)
    {
    }

    protected void SetParkingUnsuccessful()
    {
        Customer!.LeavingByCar = ProcedureState.Done;
        ParkingUnsuccessful = true;
    }

    public abstract string StrategyName { get; }

    public void Navigate(ParkingLot lot)
    {
        if (Customer!.LeavingByCar == ProcedureState.InProgress)
        {
            Customer!.LeavingByCar = ProcedureState.Done;
        }
        else if (Customer!.Parking == ProcedureState.InProgress)
        {
            if (CurrentPos.Equals(Pos.SpawnPos))
                NavigateSpawn(lot);
            else if (CurrentPos.Col == 0)
                NavigateCrossroads(lot);
            else
                NavigateInParkingLotRow(lot);
        }
        else
        {
            throw new NotSupportedException("Auto sa stratilo.");
        }
    }

    protected abstract void NavigateSpawn(ParkingLot lot);
    protected abstract void NavigateInParkingLotRow(ParkingLot lot);
    protected abstract void NavigateCrossroads(ParkingLot lot);
}