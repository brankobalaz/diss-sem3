﻿using OSPABA;

namespace entities;

public class ParkingPlace : Entity
{
    public bool IsTaken { get; private set; } = false;
    public double TakenTime { get; private set; } = 0;
    private double _lastSeizeTime;

    public double Utilization => GetUtilization();

    public ParkingPlace(Simulation mySim) : base(mySim)
    {
    }

    public ParkingPlace(int id, Simulation mySim) : base(id, mySim)
    {
    }

    public void Release()
    {
        IsTaken = false;
        TakenTime += MySim.CurrentTime - _lastSeizeTime;
    }

    public void Seize()
    {
        IsTaken = true;
        _lastSeizeTime = MySim.CurrentTime;
    }

    private double GetUtilization()
    {
        var atTime = MySim.CurrentTime;

        if (IsTaken)
            return (TakenTime + (atTime - _lastSeizeTime)) / atTime;

        return TakenTime / atTime;
    }
}