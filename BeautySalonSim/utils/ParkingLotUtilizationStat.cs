﻿using System.Collections.ObjectModel;
using entities;
using OSPStat;
using simulation;

namespace BeautySalonSim.utils;

public class ParkingLotUtilizationStat
{
    public int Rows { get; }
    public int Cols { get; }

    private readonly List<Stat> _stats = new();

    public ParkingLotUtilizationMap Map => new(this);

    public double Utilization => _stats.Average(x => x.Mean());

    public ParkingLotUtilizationStat()
    {
        Rows = Config.ParkingLotRowsCount;
        Cols = Config.ParkingLotPlacesInRowCount;

        for (var i = 0; i < Rows * Cols; i++) _stats.Add(new Stat());
    }

    public void AddSample(ParkingLot lot)
    {
        for (var i = 0; i < Rows; i++)
        for (var j = 0; j < Cols; j++)
            _stats[Cols * i + j].AddSample(lot.GetUtilizationAt(new Pos(i + 1, j + 1)));
    }

    public Stat GetStatAt(Pos pos)
    {
        return _stats[Cols * (pos.Row - 1) + (pos.Col - 1)];
    }

    public double GetRowUtilization(int rowIndex)
    {
        return _stats.Where((x, i) => rowIndex * Config.ParkingLotPlacesInRowCount < i &&
                                      i < (rowIndex + 1) * Config.ParkingLotPlacesInRowCount - 1)
            .Average(x => x.Mean());
    }

    public void ClearStats()
    {
        for (var i = 0; i < Rows * Cols; i++)
            _stats[i].Clear();
    }
}