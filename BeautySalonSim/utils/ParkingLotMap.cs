﻿using System.Collections.ObjectModel;
using entities;
using simulation;

namespace BeautySalonSim.utils;

public class ParkingLotMap : ObservableCollection<ObservableCollection<DummyCar>>
{
    public int MapRowsCount { get; } = Config.ParkingLotRowsCount + 1;
    public int MapColsCount { get; } = Config.ParkingLotPlacesInRowCount + 1;

    public ParkingLotMap(List<Car> allCars)
    {
        for (var i = 0; i < MapRowsCount; i++)
        {
            Add(new ObservableCollection<DummyCar>());
            for (var j = 0; j < MapColsCount; j++) Items[i].Add(new DummyCar());
        }

        foreach (var car in allCars)
            Items[car.CurrentPos.Row][MapColsCount - car.CurrentPos.Col - 1] = new DummyCar(car);
    }
}