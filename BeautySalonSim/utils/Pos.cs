﻿using simulation;

namespace BeautySalonSim.utils;

public class Pos
{
    public int Row { get; set; }
    public int Col { get; set; }

    public static Pos SpawnPos => new(0, Config.ParkingLotPlacesInRowCount);
    public static Pos CrossroadAPos => new(1, 0);
    public Pos NextInRowPos => new(Row, Col + 1);

    public bool IsAtEndOfRow => Col == Config.ParkingLotPlacesInRowCount;

    public Pos(int row, int col)
    {
        Row = row;
        Col = col;
    }

    public Pos(Pos pos)
    {
        Row = pos.Row;
        Col = pos.Col;
    }


    public override int GetHashCode()
    {
        return HashCode.Combine(Row, Col);
    }

    public override string ToString()
    {
        if (Equals(SpawnPos)) return "[SP]";
        if (Col == 0) return $"[{(char) ('A' - 1 + Row)}X]";
        return $"[{(char) ('A' - 1 + Row)}{Col}]";
    }

    protected bool Equals(Pos other)
    {
        return Row == other.Row && Col == other.Col;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != GetType()) return false;
        return Equals((Pos) obj);
    }
}