﻿using System.Collections.ObjectModel;
using entities;
using OSPStat;
using simulation;

namespace BeautySalonSim.utils;

public class ParkingLotUtilizationMap : ObservableCollection<ObservableCollection<double>>
{
    public int Rows { get; }
    public int Cols { get; }

    public double Utilization { get; }
    public ObservableCollection<double> RowsUtilization { get; } = new();

    public ParkingLotUtilizationMap(ParkingLotUtilizationStat stat)
    {
        Rows = Config.ParkingLotRowsCount;
        Cols = Config.ParkingLotPlacesInRowCount;

        Utilization = stat.Utilization;

        for (var i = 0; i < Rows; i++) RowsUtilization.Add(stat.GetRowUtilization(i));

        for (var i = 0; i < Rows; i++)
        {
            Items.Add(new ObservableCollection<double>());
            for (var j = 0; j < Cols; j++) Items[i].Add(stat.GetStatAt(new Pos(i + 1, Cols - j)).Mean());
        }
    }
}