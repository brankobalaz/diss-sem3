﻿namespace utils;

public enum ProcedureState
{
    NotRequested,
    Requested,
    InProgress,
    Done
}