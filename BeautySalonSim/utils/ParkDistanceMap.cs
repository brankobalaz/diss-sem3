﻿using System.Collections.ObjectModel;
using BeautySalonSim.utils;
using simulation;

namespace entities;

public static class ParkDistanceMap
{
    public static double GetCarDriveTime(Pos currPos, Pos destPos)
    {
        var currRow = currPos.Row;
        var currCol = currPos.Col;

        var destRow = destPos.Row;
        var destCol = destPos.Col;

        var slowDistance = 0.0;
        var fastDistance = 0.0;

        while (destCol != currCol || destRow != currRow)
            if (destRow != currRow && (currRow == 0 || currCol == 0) && destRow > currRow) // Step 1 - select row
            {
                fastDistance += currCol * Config.ParkColDistance;
                currCol = 0;
                while (currRow != destRow) fastDistance += Config.ParkRowDistanceList[currRow++];
            }
            else if (destRow == currRow && destCol > currCol) // Step 2 - move in row
            {
                slowDistance += (destCol - currCol) * Config.ParkColDistance;
                currCol = destCol;
            }
            else // Step 3 - to spawn
            {
                slowDistance += (Config.ParkingLotPlacesInRowCount - currCol) * Config.ParkColDistance;
                currCol = Config.ParkingLotPlacesInRowCount; // ?
                while (currRow != 0) fastDistance += Config.ParkRowDistanceList[--currRow];
            }

        if (slowDistance == 0 && fastDistance == 0)
            throw new NotImplementedException(
                "Distance calculation not implemented! (PROBABLY, MAYBE YOU JUST DO STH WRONG (CURR == DEST) !!!)");

        return fastDistance / Config.CarFastSpeed + slowDistance / Config.CarSlowSpeed;
    }

    public static double GetSalonWalkDistance(Pos pos)
    {
        var row = pos.Row;
        var col = pos.Col;
        var distance = 0.0;

        distance += (col - 1) * Config.ParkColDistance;
        while (row != 0)
            distance += Config.ParkRowDistanceList[--row];

        return distance;
    }
}