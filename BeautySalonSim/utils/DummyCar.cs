﻿using entities;

namespace BeautySalonSim.utils;

public class DummyCar
{
    public int Color { get; set; }
    public bool IsParked { get; set; }

    public DummyCar(Car car)
    {
        Color = car.Color;
        IsParked = car.IsParked;
    }

    public DummyCar()
    {
        Color = -1;
        IsParked = false;
    }
}