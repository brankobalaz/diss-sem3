using OSPABA;
using simulation;
using managers;
using continualAssistants;
using OSPStat;

namespace agents;

//meta! id="2"
public class BossAgent : Agent
{
    public static int EnvironsWithGenerateCustomersCount = 2; //2;

    public BossAgent(int id, Simulation mySim, Agent parent) :
        base(id, mySim, parent)
    {
        Init();
    }

    public int AllCustomersReturnedFlag { get; set; }

    override public void PrepareReplication()
    {
        base.PrepareReplication();

        // Setup component for the next replication
        AllCustomersReturnedFlag = 0;

        var msg1 = new MyMessage(MySim)
        {
            AddresseeId = SimId.EnvironsByFootAgent,
            Code = Mc.Begin
        };

        var msg2 = new MyMessage(MySim)
        {
            AddresseeId = SimId.EnvironsParkingLotAgent,
            Code = Mc.Begin
        };

        MyManager.Notice(msg1);
        MyManager.Notice(msg2);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void Init()
    {
        new BossManager(SimId.BossManager, MySim, this);
        AddOwnMessage(Mc.CustomerArrive);
        AddOwnMessage(Mc.CustomerServeSalon);
    }
    //meta! tag="end"
}