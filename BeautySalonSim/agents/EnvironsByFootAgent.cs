using OSPABA;
using simulation;
using managers;
using continualAssistants;
using entities;
using OSPStat;

namespace agents;

//meta! id="5"
public class EnvironsByFootAgent : Agent
{
    public Stat TimeInModelStat { get; set; } = new();
    public List<Customer> AllCustomersByFoot { get; set; } = new();
    public int ReturnedCustomersCount { get; set; }

    public EnvironsByFootAgent(int id, Simulation mySim, Agent parent) :
        base(id, mySim, parent)
    {
        Init();
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication
        ReturnedCustomersCount = 0;
        TimeInModelStat.Clear();
        AllCustomersByFoot.Clear();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void Init()
    {
        new EnvironsByFootManager(SimId.EnvironsByFootManager, MySim, this);
        new CustomerArrivalByFootScheduler(SimId.CustomerArrivalByFootScheduler, MySim, this);
        AddOwnMessage(Mc.Begin);
        AddOwnMessage(Mc.CustomerArrive);
    }
    //meta! tag="end"
}