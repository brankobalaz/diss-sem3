using OSPABA;
using simulation;
using managers;
using continualAssistants;
using entities;
using OSPDataStruct;
using OSPStat;

namespace agents;

//meta! id="22"
public class FaceServiceAgent : Agent
{
    public SimQueue<MyMessage> FaceQueue { get; }
    public Stat TimeInFaceQueueStat { get; } = new();

    public bool IsToFaceQueueEmpty => FaceQueue.IsEmpty();

    public EmployeeTeam EmployeeTeam { get; private set; }

    public FaceServiceAgent(int id, Simulation mySim, Agent parent) :
        base(id, mySim, parent)
    {
        Init();
        FaceQueue = new SimQueue<MyMessage>(new WStat(MySim));
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        FaceQueue.Clear();
        TimeInFaceQueueStat.Clear();

        // Create employees
        EmployeeTeam = new EmployeeTeam(Config.FaceEmployeeCount, MySim);
    }

    public void AddMessageToFaceQueue(MessageForm message)
    {
        var msg = (MyMessage) message;

        QueueChangeNotifySalon(1);

        msg.Customer!.EnterFaceQueueTimestamp = MySim.CurrentTime;
        FaceQueue.Enqueue(msg);
    }

    public MyMessage RemoveMessageFromFaceQueue()
    {
        var msg = FaceQueue.Dequeue();

        QueueChangeNotifySalon(-1);

        TimeInFaceQueueStat.AddSample(MySim.CurrentTime - msg.Customer!.EnterFaceQueueTimestamp);
        return msg;
    }

    private void QueueChangeNotifySalon(int change)
    {
        var notif = new MyMessage(MySim)
        {
            Code = Mc.CustomersInQueueCountChanged,
            AddresseeId = SimId.BeautySalonAgent,
            QueueSizeChange = change
        };
        MyManager.Notice(notif);
    }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		private void Init()
		{
			new FaceServiceManager(SimId.FaceServiceManager, MySim, this);
			new MakeupService(SimId.MakeupService, MySim, this);
			new FaceCleansingService(SimId.FaceCleansingService, MySim, this);
			AddOwnMessage(Mc.ServeFaceService);
		}
		//meta! tag="end"
}