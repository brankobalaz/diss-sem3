using System.Security.AccessControl;
using OSPABA;
using simulation;
using managers;
using continualAssistants;
using entities;
using OSPDataStruct;
using OSPStat;
using Action = OSPABA.Action;

namespace agents;

//meta! id="20"
public class ReceptionAgent : Agent
{
    public SimQueue<MyMessage> CheckInQueue { get; }
    public SimQueue<MyMessage> PayQueue { get; }

    public Stat TimeInCheckInQueueStat { get; } = new();
    public Stat TimeInPayQueueStat { get; } = new();

    public bool IsToPayQueueEmpty => PayQueue.IsEmpty();
    public bool IsToCheckInQueueEmpty => CheckInQueue.IsEmpty();

    public EmployeeTeam EmployeeTeam { get; private set; }

    public int CheckInCustomersInProgressCount { get; set; }
    public int ServedCustomersCount { get; set; }
    public int LeftCustomersCount { get; set; }

    public int HairFaceQueueCount;

    public bool IsPlaceForNextCustomer =>
        CheckInCustomersInProgressCount + HairFaceQueueCount <= Config.MaxCustomersInStoreCount;

    public event System.Action? OnSalonClose;

    public ReceptionAgent(int id, Simulation mySim, Agent parent) :
        base(id, mySim, parent)
    {
        Init();
        CheckInQueue = new SimQueue<MyMessage>(new WStat(MySim));
        PayQueue = new SimQueue<MyMessage>(new WStat(MySim));
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        // Clear queues
        CheckInQueue.Clear();
        PayQueue.Clear();

        //Clear statistics
        TimeInCheckInQueueStat.Clear();
        TimeInPayQueueStat.Clear();

        CheckInCustomersInProgressCount = 0;
        ServedCustomersCount = 0;
        LeftCustomersCount = 0;

        HairFaceQueueCount = 0;

        // Create employees
        EmployeeTeam = new EmployeeTeam(Config.ReceptionEmployeeCount, MySim);

        var msg = new MyMessage(MySim) {AddresseeId = SimId.CloseSalon};
        MyManager.StartContinualAssistant(msg);
    }

    public void InvokeOnSalonClose()
    {
        OnSalonClose?.Invoke();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void Init()
    {
        new ReceptionManager(SimId.ReceptionManager, MySim, this);
        new CheckInService(SimId.CheckInService, MySim, this);
        new CloseSalon(SimId.CloseSalon, MySim, this);
        new PayService(SimId.PayService, MySim, this);
        AddOwnMessage(Mc.CustomersInQueueCountChanged);
        AddOwnMessage(Mc.ServeCheckInService);
        AddOwnMessage(Mc.ServePayService);
    }
    //meta! tag="end"

    public MyMessage RemoveMessageFromCheckInQueue()
    {
        var msg = CheckInQueue.Dequeue();

        TimeInCheckInQueueStat.AddSample(MySim.CurrentTime - msg.Customer!.EnterCheckInQueueTimestamp);
        return msg;
    }

    public MyMessage RemoveMessageFromPayQueue()
    {
        var msg = PayQueue.Dequeue();

        TimeInPayQueueStat.AddSample(MySim.CurrentTime - msg.Customer!.EnterPayQueueTimestamp);
        return msg;
    }

    public void AddMessageToCheckInQueue(MessageForm message)
    {
        var msg = (MyMessage) message;

        msg.Customer!.EnterCheckInQueueTimestamp = MySim.CurrentTime;
        CheckInQueue.Enqueue(msg);
    }

    public void AddMessageToPayQueue(MessageForm message)
    {
        var msg = (MyMessage) message;

        msg.Customer!.EnterPayQueueTimestamp = MySim.CurrentTime;
        PayQueue.Enqueue(msg);
    }
}