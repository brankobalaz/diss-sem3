using OSPABA;
using simulation;
using managers;
using continualAssistants;
using entities;
using OSPDataStruct;
using OSPStat;

namespace agents;

//meta! id="21"
public class HairServiceAgent : Agent
{
    public SimQueue<MyMessage> HairQueue { get; }
    public Stat TimeInHairQueueStat { get; } = new();

    public bool IsToHairQueueEmpty => HairQueue.IsEmpty();

    public EmployeeTeam EmployeeTeam { get; private set; }

    public HairServiceAgent(int id, Simulation mySim, Agent parent) :
        base(id, mySim, parent)
    {
        Init();
        HairQueue = new SimQueue<MyMessage>(new WStat(MySim));
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        HairQueue.Clear();
        TimeInHairQueueStat.Clear();

        // Create employees
        EmployeeTeam = new EmployeeTeam(Config.HairEmployeeCount, MySim);
    }

    public void AddMessageToHairQueue(MessageForm message)
    {
        var msg = (MyMessage) message;

        QueueChangeNotifySalon(1);

        msg.Customer!.EnterHairQueueTimestamp = MySim.CurrentTime;
        HairQueue.Enqueue(msg);
    }

    public MyMessage RemoveMessageFromHairQueue()
    {
        var msg = HairQueue.Dequeue();

        QueueChangeNotifySalon(-1);

        TimeInHairQueueStat.AddSample(MySim.CurrentTime - msg.Customer!.EnterHairQueueTimestamp);
        return msg;
    }

    private void QueueChangeNotifySalon(int change)
    {
        var notif = new MyMessage(MySim)
        {
            Code = Mc.CustomersInQueueCountChanged,
            AddresseeId = SimId.BeautySalonAgent,
            QueueSizeChange = change
        };
        MyManager.Notice(notif);
    }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		private void Init()
		{
			new HairServiceManager(SimId.HairServiceManager, MySim, this);
			new HairService(SimId.HairService, MySim, this);
			AddOwnMessage(Mc.ServeHairService);
		}
		//meta! tag="end"
}