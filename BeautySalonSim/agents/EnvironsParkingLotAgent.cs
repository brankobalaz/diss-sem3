using BeautySalonSim.utils;
using OSPABA;
using simulation;
using managers;
using continualAssistants;
using entities;
using OSPStat;

namespace agents;

//meta! id="79"
public class EnvironsParkingLotAgent : Agent
{
    private int _returnedCustomersCount;
    public List<Car> AllCars { get; set; } = new();
    public List<Customer> AllCustomersByCar { get; set; } = new();

    public int ReturnedCustomersCount
    {
        get => _returnedCustomersCount;
        set
        {
            _returnedCustomersCount = value;

            if (MySim.CurrentTime > Config.SalonClosingAfterStartTime &&
                AllCustomersByCar.Count == ReturnedCustomersCount)
            {
                var newMessage = new MyMessage(MySim) {Code = Mc.AllCustomersReturned};
                MyManager.Notice(newMessage);
            }
        }
    }

    public Stat ParkingSatisfaction { get; } = new();

    public ParkingLot ParkingLot { get; set; }

    public ParkingLotMap ParkingLotMap => new(AllCars);
    public int ParkingUnsuccessfulCount { get; set; }

    public double ParkingSuccessRate =>
        AllCars.Count == 0 ? 0 : (AllCars.Count - ParkingUnsuccessfulCount) / (double) AllCars.Count;

    public EnvironsParkingLotAgent(int id, Simulation mySim, Agent parent) :
        base(id, mySim, parent)
    {
        Init();
        ParkingLot = new ParkingLot(MySim);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication
        ReturnedCustomersCount = 0;
        ParkingUnsuccessfulCount = 0;

        AllCars.Clear();
        AllCustomersByCar.Clear();
        ParkingSatisfaction.Clear();
        ParkingLot = new ParkingLot(MySim);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void Init()
    {
        new EnvironsParkingLotManager(SimId.EnvironsParkingLotManager, MySim, this);
        new Walk(SimId.Walk, MySim, this);
        new CarArrivalScheduler(SimId.CarArrivalScheduler, MySim, this);
        new Drive(SimId.Drive, MySim, this);
        AddOwnMessage(Mc.Begin);
        AddOwnMessage(Mc.CustomerArrive);
    }
    //meta! tag="end"
}