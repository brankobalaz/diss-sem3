using OSPABA;
using simulation;
using managers;
using continualAssistants;
using OSPStat;

namespace agents;

//meta! id="6"
public class BeautySalonAgent : Agent
{
    public Stat TimeInSalonStat { get; set; } = new();

    public BeautySalonAgent(int id, Simulation mySim, Agent parent) :
        base(id, mySim, parent)
    {
        Init();
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication
        TimeInSalonStat.Clear();
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void Init()
    {
        new BeautySalonManager(SimId.BeautySalonManager, MySim, this);
        AddOwnMessage(Mc.ServeHairService);
        AddOwnMessage(Mc.ServeCheckInService);
        AddOwnMessage(Mc.ServeFaceService);
        AddOwnMessage(Mc.ServePayService);
        AddOwnMessage(Mc.CustomerServeSalon);
    }
    //meta! tag="end"
}