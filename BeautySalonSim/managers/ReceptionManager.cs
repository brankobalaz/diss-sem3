using System.Data;
using OSPABA;
using simulation;
using agents;
using continualAssistants;
using utils;

namespace managers;

//meta! id="20"
public class ReceptionManager : Manager
{
    public ReceptionManager(int id, Simulation mySim, Agent myAgent) :
        base(id, mySim, myAgent)
    {
        Init();
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        if (PetriNet != null) PetriNet.Clear();
    }

    //meta! sender="CheckInService", id="31", type="Finish"
    public void ProcessFinishCheckInService(MessageForm message)
    {
        var msg = (MyMessage) message;

        msg.Employee!.Release();
        msg.Employee = null;

        msg.Customer!.CheckInProcedure = ProcedureState.Done;
        MyAgent.CheckInCustomersInProgressCount--;

        TryStartNewServiceAtReception();

        msg.Code = Mc.ServeCheckInService;
        Response(msg);
    }

    //meta! sender="PayService", id="33", type="Finish"
    public void ProcessFinishPayService(MessageForm message)
    {
        MyAgent.ServedCustomersCount++;

        var msg = (MyMessage) message;

        msg.Employee!.Release();
        msg.Employee = null;

        TryStartNewServiceAtReception();

        msg.Customer!.PayProcedure = ProcedureState.Done;
        msg.Code = Mc.ServePayService;
        Response(msg);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
        }
    }

    //meta! sender="BeautySalonAgent", id="47", type="Request"
    public void ProcessServeCheckInService(MessageForm message)
    {
        MyAgent.AddMessageToCheckInQueue(message);
        TryStartNewServiceAtReception();
    }

    private void TryStartNewServiceAtReception()
    {
        if (!MyAgent.IsToPayQueueEmpty) // Platiaci zákazníci majú prednosť
        {
            var receptionEmployee = MyAgent.EmployeeTeam.GetNotBusyEmployee();
            if (receptionEmployee is null) // Nie je voľný zamestnanec - nezoberie ani z check in radu
                return;

            receptionEmployee.Seize();

            var msg = MyAgent.RemoveMessageFromPayQueue();
            msg.Employee = receptionEmployee;
            msg.Customer!.PayProcedure = ProcedureState.InProgress;

            msg.AddresseeId = SimId.PayService;
            StartContinualAssistant(msg);
        }

        // Is place for next customer
        if (!MyAgent.IsPlaceForNextCustomer)
            return;

        // Customer from check-in queue
        if (!MyAgent.IsToCheckInQueueEmpty)
        {
            var receptionEmployee = MyAgent.EmployeeTeam.GetNotBusyEmployee();
            if (receptionEmployee is null)
                return;

            receptionEmployee.Seize();

            var msg = MyAgent.RemoveMessageFromCheckInQueue();
            msg.Employee = receptionEmployee;
            msg.Customer!.CheckInProcedure = ProcedureState.InProgress;

            MyAgent.CheckInCustomersInProgressCount++;

            msg.AddresseeId = SimId.CheckInService;
            StartContinualAssistant(msg);
        }
    }

    //meta! sender="BeautySalonAgent", id="52", type="Request"
    public void ProcessServePayService(MessageForm message)
    {
        MyAgent.AddMessageToPayQueue(message);
        TryStartNewServiceAtReception();
    }

    //meta! sender="CloseSalon", id="64", type="Finish"
    public void ProcessFinishCloseSalon(MessageForm message)
    {
        MyAgent.LeftCustomersCount = MyAgent.CheckInQueue.Count;

        MyAgent.InvokeOnSalonClose();

        while (!MyAgent.CheckInQueue.IsEmpty())
        {
            var msg = MyAgent.CheckInQueue.Dequeue(); // pretože nechcem ich zarátavať do štatistík

            msg.Customer!.IsLeavingWithoutServeInSalon = true;

            msg.Code = Mc.ServeCheckInService;
            Response(msg);
        }
    }

    //meta! sender="BeautySalonAgent", id="76", type="Notice"
    public void ProcessCustomersInQueueCountChanged(MessageForm message)
    {
        var msg = (MyMessage) message;

        MyAgent.HairFaceQueueCount += msg.QueueSizeChange ?? int.MaxValue;
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void Init()
    {
    }

    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.ServeCheckInService:
                ProcessServeCheckInService(message);
                break;

            case IdList.Finish:
                switch (message.Sender.Id)
                {
                    case SimId.CloseSalon:
                        ProcessFinishCloseSalon(message);
                        break;

                    case SimId.PayService:
                        ProcessFinishPayService(message);
                        break;

                    case SimId.CheckInService:
                        ProcessFinishCheckInService(message);
                        break;
                }

                break;

            case Mc.CustomersInQueueCountChanged:
                ProcessCustomersInQueueCountChanged(message);
                break;

            case Mc.ServePayService:
                ProcessServePayService(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new ReceptionAgent MyAgent => (ReceptionAgent) base.MyAgent;
}