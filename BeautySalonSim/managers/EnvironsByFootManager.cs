using OSPABA;
using simulation;
using agents;
using continualAssistants;

namespace managers;

//meta! id="5"
public class EnvironsByFootManager : Manager
{
    public EnvironsByFootManager(int id, Simulation mySim, Agent myAgent) :
        base(id, mySim, myAgent)
    {
        Init();
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        if (PetriNet != null) PetriNet.Clear();
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
        }
    }

    //meta! sender="CustomerArrivalByFootScheduler", id="16", type="Finish"
    public void ProcessFinish(MessageForm message)
    {
        message.Code = Mc.CustomerArrive;
        message.AddresseeId = SimId.BossAgent;

        Request(message);
    }

    //meta! sender="BossAgent", id="45", type="Notice"
    public void ProcessBegin(MessageForm message)
    {
        // Initialise simulation

        message.AddresseeId = SimId.CustomerArrivalByFootScheduler;
        StartContinualAssistant(message);
    }

    //meta! sender="BossAgent", id="44", type="Response"
    public void ProcessCustomerArrive(MessageForm message)
    {
        var msg = (MyMessage) message;
        var customer = msg.Customer!;

        if (!customer.IsLeavingWithoutServeInSalon)
            MyAgent.TimeInModelStat.AddSample(MySim.CurrentTime - customer!.EnterSalonTimestamp);

        MyAgent.ReturnedCustomersCount++;

        if (MySim.CurrentTime > Config.SalonClosingAfterStartTime &&
            MyAgent.AllCustomersByFoot.Count == MyAgent.ReturnedCustomersCount)
        {
            var newMessage = new MyMessage(MySim)
            {
                Code = Mc.AllCustomersReturned
            };
            Notice(newMessage);
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void Init()
    {
    }

    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.CustomerArrive:
                ProcessCustomerArrive(message);
                break;

            case Mc.Begin:
                ProcessBegin(message);
                break;

            case IdList.Finish:
                ProcessFinish(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new EnvironsByFootAgent MyAgent => (EnvironsByFootAgent) base.MyAgent;
}