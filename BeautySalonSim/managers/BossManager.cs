using OSPABA;
using simulation;
using agents;
using continualAssistants;

namespace managers;

//meta! id="2"
public class BossManager : Manager
{
    public BossManager(int id, Simulation mySim, Agent myAgent) :
        base(id, mySim, myAgent)
    {
        Init();

        MyAgent.AddOwnMessage(Mc.AllCustomersReturned);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        if (PetriNet != null) PetriNet.Clear();
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.AllCustomersReturned:
                ProcessAllCustomersReturned(message);
                break;
        }
    }

    private void ProcessAllCustomersReturned(MessageForm message)
    {
        MyAgent.AllCustomersReturnedFlag++;
        if (MyAgent.AllCustomersReturnedFlag == BossAgent.EnvironsWithGenerateCustomersCount) MySim.StopReplication();
    }

    //meta! sender="EnvironsByFootAgent", id="44", type="Request"
    public void ProcessCustomerArriveEnvironsByFootAgent(MessageForm message)
    {
        SendCustomerServeInSalon(message);
    }

    //meta! sender="BeautySalonAgent", id="46", type="Response"
    public void ProcessCustomerServeSalon(MessageForm message)
    {
        message.Code = Mc.CustomerArrive;
        Response(message);
    }

    //meta! sender="EnvironsParkingLotAgent", id="82", type="Request"
    public void ProcessCustomerArriveEnvironsParkingLotAgent(MessageForm message)
    {
        SendCustomerServeInSalon(message);
    }

    private void SendCustomerServeInSalon(MessageForm message)
    {
        message.Code = Mc.CustomerServeSalon;
        message.AddresseeId = SimId.BeautySalonAgent;

        Request(message);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void Init()
    {
    }

    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.CustomerServeSalon:
                ProcessCustomerServeSalon(message);
                break;

            case Mc.CustomerArrive:
                switch (message.Sender.Id)
                {
                    case SimId.EnvironsParkingLotAgent:
                        ProcessCustomerArriveEnvironsParkingLotAgent(message);
                        break;

                    case SimId.EnvironsByFootAgent:
                        ProcessCustomerArriveEnvironsByFootAgent(message);
                        break;
                }

                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new BossAgent MyAgent => (BossAgent) base.MyAgent;
}