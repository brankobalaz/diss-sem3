using OSPABA;
using simulation;
using agents;
using continualAssistants;
using entities;
using utils;

namespace managers;

//meta! id="6"
public class BeautySalonManager : Manager
{
    public BeautySalonManager(int id, Simulation mySim, Agent myAgent) :
        base(id, mySim, myAgent)
    {
        Init();
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        if (PetriNet != null) PetriNet.Clear();
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
        }
    }

    //meta! sender="HairServiceAgent", id="48", type="Response"
    public void ProcessServeHairService(MessageForm message)
    {
        RouteMessage(message);
    }

    //meta! sender="FaceServiceAgent", id="50", type="Response"
    public void ProcessServeFaceService(MessageForm message)
    {
        RouteMessage(message);
    }

    //meta! sender="ReceptionAgent", id="47", type="Response"
    public void ProcessServeCheckInService(MessageForm message)
    {
        RouteMessage(message);
    }

    private void RouteMessage(MessageForm message)
    {
        var msg = (MyMessage) message;
        var customer = msg.Customer!;

        if (customer.IsLeavingWithoutServeInSalon)
        {
            message.Code = Mc.CustomerServeSalon;
            Response(message);
        }
        else
        {
            if (customer.CheckInProcedure == ProcedureState.Requested)
            {
                msg.Code = Mc.ServeCheckInService;
                msg.AddresseeId = SimId.ReceptionAgent;
            }
            else if (customer.HairProcedure == ProcedureState.Requested)
            {
                msg.Code = Mc.ServeHairService;
                msg.AddresseeId = SimId.HairServiceAgent;
            }
            else if (customer.FaceCleansingProcedure == ProcedureState.Requested ||
                     customer.MakeupProcedure == ProcedureState.Requested)
            {
                msg.Code = Mc.ServeFaceService;
                msg.AddresseeId = SimId.FaceServiceAgent;
            }
            else if (customer.PayProcedure == ProcedureState.Requested)
            {
                msg.Code = Mc.ServePayService;
                msg.AddresseeId = SimId.ReceptionAgent;
            }

            Request(msg);
        }
    }

    //meta! sender="BossAgent", id="46", type="Request"
    public void ProcessCustomerServeSalon(MessageForm message)
    {
        var msg = (MyMessage) message;

        if (MySim.CurrentTime < Config.SalonClosingAfterStartTime)
            msg.Customer!.EnterSalonTimestamp = MySim.CurrentTime;
        else
            msg.Customer!.IsLeavingWithoutServeInSalon = true;

        RouteMessage(message);
    }

    //meta! sender="ReceptionAgent", id="52", type="Response"
    public void ProcessServePayService(MessageForm message)
    {
        var customer = ((MyMessage) message).Customer!;
        MyAgent.TimeInSalonStat.AddSample(MySim.CurrentTime - customer.EnterSalonTimestamp);

        message.Code = Mc.CustomerServeSalon;
        Response(message);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    public void Init()
    {
    }

    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.CustomerServeSalon:
                ProcessCustomerServeSalon(message);
                break;

            case Mc.ServeCheckInService:
                ProcessServeCheckInService(message);
                break;

            case Mc.ServeHairService:
                ProcessServeHairService(message);
                break;

            case Mc.ServeFaceService:
                ProcessServeFaceService(message);
                break;

            case Mc.ServePayService:
                ProcessServePayService(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new BeautySalonAgent MyAgent => (BeautySalonAgent) base.MyAgent;
}