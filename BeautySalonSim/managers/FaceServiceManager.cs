using OSPABA;
using simulation;
using agents;
using continualAssistants;
using entities;
using utils;

namespace managers;

//meta! id="22"
public class FaceServiceManager : Manager
{
    public FaceServiceManager(int id, Simulation mySim, Agent myAgent) :
        base(id, mySim, myAgent)
    {
        Init();
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        if (PetriNet != null) PetriNet.Clear();
    }

		//meta! sender="FaceCleansingService", id="40", type="Finish"
		public void ProcessFinishFaceCleansingService(MessageForm message)
    {
        ((MyMessage) message).Customer!.FaceCleansingProcedure = ProcedureState.Done;
        ProcessDoneServe(message);
    }

		//meta! sender="MakeupService", id="42", type="Finish"
		public void ProcessFinishMakeupService(MessageForm message)
    {
        ((MyMessage) message).Customer!.MakeupProcedure = ProcedureState.Done;
        ProcessDoneServe(message);
    }

    private void ProcessDoneServe(MessageForm message)
    {
        var msg = (MyMessage) message;

        msg.Employee!.Release();
        msg.Employee = null;

        TryStartNewServiceAtFaceStation();

        msg.Code = Mc.ServeFaceService;
        Response(msg);
    }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
        }
    }

		//meta! sender="BeautySalonAgent", id="50", type="Request"
		public void ProcessServeFaceService(MessageForm message)
    {
        MyAgent.AddMessageToFaceQueue(message);
        TryStartNewServiceAtFaceStation();
    }

    public void TryStartNewServiceAtFaceStation()
    {
        if (MyAgent.IsToFaceQueueEmpty)
            return;

        var notBusyEmployee = MyAgent.EmployeeTeam.GetNotBusyEmployee();
        if (notBusyEmployee is null)
            return;

        notBusyEmployee.Seize();

        var msg = MyAgent.RemoveMessageFromFaceQueue();
        msg.Employee = notBusyEmployee;
        var customer = msg.Customer;


        if (customer!.FaceCleansingProcedure == ProcedureState.Requested)
        {
            customer.FaceCleansingProcedure = ProcedureState.InProgress;
            msg.AddresseeId = SimId.FaceCleansingService;
        }

        else
        {
            customer.MakeupProcedure = ProcedureState.InProgress;
            msg.AddresseeId = SimId.MakeupService;
        }

        StartContinualAssistant(msg);
    }


		//meta! userInfo="Generated code: do not modify", tag="begin"
		public void Init()
		{
		}

		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Finish:
				switch (message.Sender.Id)
				{
				case SimId.FaceCleansingService:
					ProcessFinishFaceCleansingService(message);
				break;

				case SimId.MakeupService:
					ProcessFinishMakeupService(message);
				break;
				}
			break;

			case Mc.ServeFaceService:
				ProcessServeFaceService(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
    public new FaceServiceAgent MyAgent => (FaceServiceAgent) base.MyAgent;
}