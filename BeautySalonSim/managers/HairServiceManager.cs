using OSPABA;
using simulation;
using agents;
using continualAssistants;
using utils;

namespace managers;

//meta! id="21"
public class HairServiceManager : Manager
{
    public HairServiceManager(int id, Simulation mySim, Agent myAgent) :
        base(id, mySim, myAgent)
    {
        Init();
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        if (PetriNet != null) PetriNet.Clear();
    }

		//meta! sender="HairService", id="37", type="Finish"
		public void ProcessFinish(MessageForm message)
    {
        var msg = (MyMessage) message;

        msg.Employee!.Release();
        msg.Employee = null;

        msg.Customer!.HairProcedure = ProcedureState.Done;

        TryStartNewServiceAtHairStation();

        msg.Code = Mc.ServeHairService;
        Response(msg);
    }

    private void TryStartNewServiceAtHairStation()
    {
        if (MyAgent.IsToHairQueueEmpty)
            return;

        var notBusyEmployee = MyAgent.EmployeeTeam.GetNotBusyEmployee();
        if (notBusyEmployee is null)
            return;

        notBusyEmployee.Seize();

        var msg = MyAgent.RemoveMessageFromHairQueue();
        msg.Employee = notBusyEmployee;
        msg.Customer!.WashingProcedure = ProcedureState.InProgress;

        msg.AddresseeId = SimId.HairService;
        StartContinualAssistant(msg);
    }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
        }
    }

		//meta! sender="BeautySalonAgent", id="48", type="Request"
		public void ProcessServeHairService(MessageForm message)
    {
        MyAgent.AddMessageToHairQueue(message);
        TryStartNewServiceAtHairStation();
    }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		public void Init()
		{
		}

		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Finish:
				ProcessFinish(message);
			break;

			case Mc.ServeHairService:
				ProcessServeHairService(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
    public new HairServiceAgent MyAgent => (HairServiceAgent) base.MyAgent;
}