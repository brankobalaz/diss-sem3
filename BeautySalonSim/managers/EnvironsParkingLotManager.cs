using OSPABA;
using simulation;
using agents;
using BeautySalonSim.utils;
using continualAssistants;
using utils;

namespace managers;

//meta! id="79"
public class EnvironsParkingLotManager : Manager
{
    public EnvironsParkingLotManager(int id, Simulation mySim, Agent myAgent) :
        base(id, mySim, myAgent)
    {
        Init();
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        if (PetriNet != null) PetriNet.Clear();
    }

    //meta! sender="BossAgent", id="84", type="Notice"
    public void ProcessBegin(MessageForm message)
    {
        message.AddresseeId = SimId.CarArrivalScheduler;
        StartContinualAssistant(message);
    }

    //meta! sender="Drive", id="86", type="Finish"
    public void ProcessFinishDrive(MessageForm message)
    {
        var msg = (MyMessage) message;
        var customer = msg.Car!.Customer!;
        var car = msg.Car!;

        car.Navigate(MyAgent.ParkingLot); // Make decision

        if (customer.LeavingByCar == ProcedureState.Done) // Odišiel 
        {
            // END IN MODEL
            if (car.ParkingUnsuccessful) MyAgent.ParkingUnsuccessfulCount++;
            MyAgent.ReturnedCustomersCount++;
        }
        else if (customer.Parking == ProcedureState.Done) // Zaparkoval
        {
            msg.Car = null;
            msg.Customer = customer;

            var ps = car.SatisfactionPenaltyCounter * Config.ParkingPenalty;
            ps += (car.CurrentPos.Row - 1) * Config.ParkingLotPlacesInRowCount + Config.ParkingLotPlacesInRowCount -
                car.CurrentPos.Col + 1;

            MyAgent.ParkingSatisfaction.AddSample(ps);

            customer.WalkToSalon = ProcedureState.InProgress;
            customer.CheckInProcedure = ProcedureState.Requested;
            customer.WalkToCar = ProcedureState.Requested;
            customer.LeavingByCar = ProcedureState.Requested;

            msg.AddresseeId = SimId.Walk;
            StartContinualAssistant(msg);
        }
        else if (customer.Parking == ProcedureState.InProgress) // Nezaparkoval
        {
            msg.AddresseeId = SimId.Drive; // Skús zaparkovať inde
            StartContinualAssistant(msg);
        }
    }

//meta! sender="BossAgent", id="82", type="Response"
    public void ProcessCustomerArrive(MessageForm message)
    {
        var msg = (MyMessage) message;

        msg.Customer!.WalkToCar = ProcedureState.InProgress;
        msg.AddresseeId = SimId.Walk;
        StartContinualAssistant(msg);
    }

//meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
        }
    }

//meta! sender="CarArrivalScheduler", id="89", type="Finish"
    public void ProcessFinishCarArrivalScheduler(MessageForm message)
    {
        var msg = (MyMessage) message;

        msg.Car!.Customer!.Parking = ProcedureState.InProgress;
        msg.Car!.DestinationPos = Pos.CrossroadAPos;
        msg.AddresseeId = SimId.Drive;
        StartContinualAssistant(msg);
    }

//meta! sender="Walk", id="96", type="Finish"
    public void ProcessFinishWalk(MessageForm message)
    {
        var msg = (MyMessage) message;
        var customer = msg.Customer!;

        if (customer.WalkToCar == ProcedureState.Done)
        {
            var car = customer.Car!;
            msg.Car = car;
            msg.Customer = null;

            MyAgent.ParkingLot.ReleaseCurrPos(car);

            customer.LeavingByCar = ProcedureState.InProgress;
            car.DestinationPos = Pos.SpawnPos;
            msg.AddresseeId = SimId.Drive;

            StartContinualAssistant(msg);
        }
        else if (msg.Customer!.WalkToSalon == ProcedureState.Done)
        {
            msg.Code = Mc.CustomerArrive;
            msg.AddresseeId = SimId.BossAgent;

            Request(msg);
        }
    }

//meta! userInfo="Generated code: do not modify", tag="begin"
    public void Init()
    {
    }

    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.Begin:
                ProcessBegin(message);
                break;

            case IdList.Finish:
                switch (message.Sender.Id)
                {
                    case SimId.Walk:
                        ProcessFinishWalk(message);
                        break;

                    case SimId.Drive:
                        ProcessFinishDrive(message);
                        break;

                    case SimId.CarArrivalScheduler:
                        ProcessFinishCarArrivalScheduler(message);
                        break;
                }

                break;

            case Mc.CustomerArrive:
                ProcessCustomerArrive(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

//meta! tag="end"
    public new EnvironsParkingLotAgent MyAgent => (EnvironsParkingLotAgent) base.MyAgent;
}