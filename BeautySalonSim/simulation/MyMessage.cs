using entities;
using OSPABA;

namespace simulation;

public class MyMessage : MessageForm
{
    public Car? Car;
    public Customer? Customer;
    public Employee? Employee;
    public int? QueueSizeChange;
    public int? HairFaceQueueLength;

    public MyMessage(Simulation sim) :
        base(sim)
    {
    }

    public MyMessage(MyMessage original) :
        base(original)
    {
        // copy() is called in superclass
    }

    override public MessageForm CreateCopy()
    {
        return new MyMessage(this);
    }

    override protected void Copy(MessageForm message)
    {
        base.Copy(message);
        var original = (MyMessage) message;
        // Copy attributes

        Customer = original.Customer;
        Employee = original.Employee;
        QueueSizeChange = original.QueueSizeChange;
        HairFaceQueueLength = original.HairFaceQueueLength;
    }
}