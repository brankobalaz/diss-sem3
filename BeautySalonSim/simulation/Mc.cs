using OSPABA;

namespace simulation;

public class Mc : IdList
{
    //meta! userInfo="Generated code: do not modify", tag="begin"
    public const int CustomersInQueueCountChanged = 1016;
    public const int CustomerArrive = 1001;
    public const int Begin = 1002;
    public const int CustomerServeSalon = 1003;
    public const int ServeCheckInService = 1004;
    public const int ServeHairService = 1005;
    public const int ServeFaceService = 1007;

    public const int ServePayService = 1008;
    //meta! tag="end"

    // 1..1000 range reserved for user
    public const int CheckInServiceDone = 1;
    public const int CustomerByFootGenerated = 2;
    public const int FaceCleansingServiceDone = 3;
    public const int HairServiceDone = 4;
    public const int MakeupServiceDone = 5;
    public const int PayServiceDone = 6;
    public const int CloseSalonOccurred = 7;
    public const int CarGenerated = 8;
    public const int DriveDone = 9;
    public const int WalkDone = 10;
    public const int AllCustomersReturned = 11;
    public const int WashingDone = 12;
}