﻿using entities;

namespace simulation;

public class Config
{
    // Mics
    public static double SalonOpenClockSeconds { get; set; } = 9 * 60 * 60;
    public static double SalonClosingAfterStartTime { get; set; } = 8 * 60 * 60;

    public static int MaxCustomersInStoreCount { get; set; } = 10;
    public static double CustomerRateMultiplier { get; set; } = 1.0;

    public static double HairWashingTime { get; set; } = 10 * 60;

    // Employees
    public static int ReceptionEmployeeCount { get; set; } = 2;
    public static int HairEmployeeCount { get; set; } = 6;
    public static int FaceEmployeeCount { get; set; } = 6;

    // Parking lot
    public static int ParkingLotRowsCount { get; set; } = 3;
    public static int ParkingLotPlacesInRowCount { get; set; } = 15;

    public static List<double> ParkRowDistanceList { get; set; } = new() {13, 10, 8};
    public static double ParkColDistance { get; set; } = 35.0 / 15;

    public static double CarSlowSpeed { get; set; } = 12 / 3.6; // m/s
    public static double CarFastSpeed { get; set; } = 20 / 3.6; // m/s

    public static double WalkSpeedMin { get; set; } = 1.8;
    public static double WalkSpeedMax { get; set; } = 3.2;

    public static int ParkingPenalty { get; set; } = 50;

    public static List<Car> ParkingStrategies { get; set; } = new();
    public static int SelectedParkingStrategy { get; set; } = 2;

    public static int ColBias { get; set; } = 11;
    public static double RowBias { get; set; } = 0.35;

    // Generators
    public static double CustomerInterArrivalTimeMean = 720.0;
    public static double CarInterArrivalTimeMean = 450.0;

    public static double CheckInServiceTimeMin { get; set; } = 80;
    public static double CheckInServiceTimeMax { get; set; } = 320;
    public static double PayServiceTimeMax { get; set; } = 130;
    public static double PayServiceTimeMin { get; set; } = 230;
    public static double FaceCleansingServiceTimeMin { get; set; } = 360;
    public static double FaceCleansingServiceTimeMode { get; set; } = 540;
    public static double FaceCleansingServiceTimeMax { get; set; } = 900;

    public static double SimpleHairProcedureRate { get; set; } = 0.4;
    public static double ComplexHairProcedureRate { get; set; } = 0.4;
    public static int SecondsInMinute { get; set; } = 60;
    public static double ComplexMakeupProcedureRate { get; set; } = 0.70;
    public static double HairAndFaceProceduresRate { get; set; } = 0.65;
    public static double HairProcedureRate { get; set; } = 0.20;
    public static double FaceCleansingProcedureRate { get; set; } = 0.35;
}