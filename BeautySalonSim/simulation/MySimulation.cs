using System.ComponentModel;
using System.Globalization;
using OSPABA;
using agents;
using BeautySalonSim.utils;
using entities;
using OSPRNG;
using OSPStat;

namespace simulation;

public class MySimulation : Simulation, IDisposable
{
    private readonly Random _seedGenerator = new();
    private bool _shouldBeSlow = false;
    private Tuple<double, double> _simSpeed;

    public Stat TimeInSystemStat { get; } = new();
    public Stat TimeInSystemAtClosingStat { get; } = new();
    public Stat TimeInSalonStat { get; } = new();
    public Stat TimeInSalonAtClosingStat { get; } = new();
    public Stat ServedCustomersStat { get; } = new();
    public Stat LeftCustomersCountStat { get; } = new();
    public Stat CoolDownTimeStat { get; } = new();
    public Stat ParkingSuccessRateStat { get; } = new();
    public Stat ParkingSatisfactionStat { get; } = new();

    public Stat TimeInCheckInFrontAtClosing { get; } = new();
    public Stat TimeInCheckInFrontAtModelEnd { get; } = new();
    public Stat TimeInHairFrontAtClosing { get; } = new();
    public Stat TimeInFaceFrontAtClosing { get; } = new();
    public Stat TimeInPayFrontAtClosing { get; } = new();

    public Stat CheckInFrontLengthAtClosingStat { get; } = new();
    public Stat HairFrontLengthAtClosingStat { get; } = new();
    public Stat FaceFrontLengthAtClosingStat { get; } = new();
    public Stat PayFrontLengthAtClosingStat { get; } = new();

    public Stat ReceptionTeamSeizedCount { get; } = new();
    public Stat HairTeamSeizedCount { get; } = new();
    public Stat FaceTeamSeizedCount { get; } = new();

    public ParkingLotUtilizationStat? ParkingLotUtilizationStat { get; private set; }
    public ParkingLotUtilizationMap ParkingLotUtilizationMap => ParkingLotUtilizationStat.Map;

    public MySimulation()
    {
        Init();

        Config.ParkingStrategies.Add(new NaiveCar(this));
        Config.ParkingStrategies.Add(new BasicCar(this));
        Config.ParkingStrategies.Add(new LuckyCar(this));

        ReceptionAgent.OnSalonClose += SampleStatsOnSalonClose;
    }

    override protected void PrepareSimulation()
    {
        base.PrepareSimulation();

        // Initialise global randomness
        RNG<double>.SetSeedGen(_seedGenerator);
        RNG<int>.SetSeedGen(_seedGenerator);

        // Create global statistcis
        TimeInSystemStat.Clear();
        TimeInSystemAtClosingStat.Clear();
        TimeInSalonStat.Clear();
        TimeInSalonAtClosingStat.Clear();
        ServedCustomersStat.Clear();
        LeftCustomersCountStat.Clear();
        CoolDownTimeStat.Clear();
        ParkingSuccessRateStat.Clear();
        ParkingSatisfactionStat.Clear();

        TimeInCheckInFrontAtClosing.Clear();
        TimeInCheckInFrontAtModelEnd.Clear();
        TimeInHairFrontAtClosing.Clear();
        TimeInFaceFrontAtClosing.Clear();
        TimeInPayFrontAtClosing.Clear();

        CheckInFrontLengthAtClosingStat.Clear();
        HairFrontLengthAtClosingStat.Clear();
        FaceFrontLengthAtClosingStat.Clear();
        PayFrontLengthAtClosingStat.Clear();

        ParkingLotUtilizationStat = new ParkingLotUtilizationStat();

        ReceptionTeamSeizedCount.Clear();
        HairTeamSeizedCount.Clear();
        FaceTeamSeizedCount.Clear();
    }

    override protected void PrepareReplication()
    {
        base.PrepareReplication();
        // Reset entities, queues, local statistics, etc...
    }


    override protected void ReplicationFinished()
    {
        // Collect local statistics into global, update UI, etc...
        TimeInSystemStat.AddSample(EnvironsByFootAgent.TimeInModelStat.Mean());
        ServedCustomersStat.AddSample(ReceptionAgent.ServedCustomersCount);
        LeftCustomersCountStat.AddSample(ReceptionAgent.LeftCustomersCount);
        CoolDownTimeStat.AddSample(CurrentTime - Config.SalonClosingAfterStartTime);
        TimeInCheckInFrontAtModelEnd.AddSample(ReceptionAgent.TimeInCheckInQueueStat.Mean());
        TimeInSalonStat.AddSample(BeautySalonAgent.TimeInSalonStat.Mean());
        ParkingSuccessRateStat.AddSample(EnvironsParkingLotAgent.ParkingSuccessRate);
        ParkingSatisfactionStat.AddSample(EnvironsParkingLotAgent.ParkingSatisfaction.Mean());

        base.ReplicationFinished();
    }

    public void SampleStatsOnSalonClose()
    {
        // At 17:00 statistics
        TimeInSystemAtClosingStat.AddSample(EnvironsByFootAgent.TimeInModelStat.Mean());
        TimeInSalonAtClosingStat.AddSample(BeautySalonAgent.TimeInSalonStat.Mean());
        TimeInCheckInFrontAtClosing.AddSample(ReceptionAgent.TimeInCheckInQueueStat.Mean());
        TimeInHairFrontAtClosing.AddSample(HairServiceAgent.TimeInHairQueueStat.Mean());
        TimeInFaceFrontAtClosing.AddSample(FaceServiceAgent.TimeInFaceQueueStat.Mean());
        TimeInPayFrontAtClosing.AddSample(ReceptionAgent.TimeInPayQueueStat.Mean());

        ReceptionAgent.CheckInQueue.LengthStatistic.AddSample(ReceptionAgent.CheckInQueue.Count);
        HairServiceAgent.HairQueue.LengthStatistic.AddSample(HairServiceAgent.HairQueue.Count);
        FaceServiceAgent.FaceQueue.LengthStatistic.AddSample(FaceServiceAgent.FaceQueue.Count);
        ReceptionAgent.PayQueue.LengthStatistic.AddSample(ReceptionAgent.PayQueue.Count);

        CheckInFrontLengthAtClosingStat.AddSample(ReceptionAgent.CheckInQueue.LengthStatistic.Mean());
        HairFrontLengthAtClosingStat.AddSample(HairServiceAgent.HairQueue.LengthStatistic.Mean());
        FaceFrontLengthAtClosingStat.AddSample(FaceServiceAgent.FaceQueue.LengthStatistic.Mean());
        PayFrontLengthAtClosingStat.AddSample(ReceptionAgent.PayQueue.LengthStatistic.Mean());

        ParkingLotUtilizationStat!.AddSample(EnvironsParkingLotAgent.ParkingLot);

        ReceptionTeamSeizedCount.AddSample(ReceptionAgent.EmployeeTeam.Utilization *
                                           Config.ReceptionEmployeeCount);
        HairTeamSeizedCount.AddSample(HairServiceAgent.EmployeeTeam.Utilization * Config.HairEmployeeCount);
        FaceTeamSeizedCount.AddSample(FaceServiceAgent.EmployeeTeam.Utilization * Config.FaceEmployeeCount);
    }

    public void SafeSetSimSpeed(double interval, double duration)
    {
        _simSpeed = new Tuple<double, double>(interval, duration);
        _shouldBeSlow = true;
        SetSimSpeed(_simSpeed.Item1, _simSpeed.Item2);
    }

    public void SafeSetMaxSimSpeed()
    {
        _shouldBeSlow = false;
        SetMaxSimSpeed();
    }

    protected override void RunReplication()
    {
        if (_shouldBeSlow)
            SetSimSpeed(_simSpeed.Item1, _simSpeed.Item2, 0);

        base.RunReplication();
    }

    override protected void SimulationFinished()
    {
        base.SimulationFinished();

        WhiteResultsToFile();
    }

    private async void WhiteResultsToFile()
    {
        if (CurrentReplication <= 2)
            return;

        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("sk");

        var results = $"repl pocet: {CurrentReplication};;;;\n"
                      + "item;time_1;time_2;num_1;num_2\n"
                      + $"cas v salone (do konca);{TimeSpan.FromSeconds(TimeInSalonStat.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(TimeInSalonStat.ConfidenceInterval90[1]).ToString("g")};;\n"
                      + $"cas v salone (do 17:00);{TimeSpan.FromSeconds(TimeInSalonAtClosingStat.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(TimeInSalonAtClosingStat.ConfidenceInterval90[1]).ToString("g")};;\n"
                      + $"chladnutie;{TimeSpan.FromSeconds(CoolDownTimeStat.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(CoolDownTimeStat.ConfidenceInterval90[1]).ToString("g")};;\n"
                      + $"cas v check-in;{TimeSpan.FromSeconds(TimeInCheckInFrontAtModelEnd.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(TimeInCheckInFrontAtModelEnd.ConfidenceInterval90[1]).ToString("g")};;\n"
                      + $"cas hair;{TimeSpan.FromSeconds(TimeInHairFrontAtClosing.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(TimeInHairFrontAtClosing.ConfidenceInterval90[1]).ToString("g")};;\n"
                      + $"cas face;{TimeSpan.FromSeconds(TimeInFaceFrontAtClosing.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(TimeInFaceFrontAtClosing.ConfidenceInterval90[1]).ToString("g")};;\n"
                      + $"obsluzenych;;;{ServedCustomersStat.ConfidenceInterval90[0]};{ServedCustomersStat.ConfidenceInterval90[1]}\n"
                      + $"NEobsluzenych;;;{LeftCustomersCountStat.ConfidenceInterval90[0]};{LeftCustomersCountStat.ConfidenceInterval90[1]}\n"
                      + $"front check in;;;{CheckInFrontLengthAtClosingStat.ConfidenceInterval90[0]};{CheckInFrontLengthAtClosingStat.ConfidenceInterval90[1]}\n"
                      + $"front hair;;;{HairFrontLengthAtClosingStat.ConfidenceInterval90[0]};{HairFrontLengthAtClosingStat.ConfidenceInterval90[1]}\n"
                      + $"front face;;;{FaceFrontLengthAtClosingStat.ConfidenceInterval90[0]};{FaceFrontLengthAtClosingStat.ConfidenceInterval90[1]}\n"
                      + $"front pay;;;{PayFrontLengthAtClosingStat.ConfidenceInterval90[0]};{PayFrontLengthAtClosingStat.ConfidenceInterval90[1]}\n"
                      + $"park spokojnost;;;{ParkingSatisfactionStat.ConfidenceInterval90[0]};{ParkingSatisfactionStat.ConfidenceInterval90[1]}\n"
                      + $"park uspesnost;;;{ParkingSuccessRateStat.ConfidenceInterval90[0]};{ParkingSuccessRateStat.ConfidenceInterval90[1]}\n"
                      + $"ppp recepcne;;;{ReceptionTeamSeizedCount.ConfidenceInterval90[0]};{ReceptionTeamSeizedCount.ConfidenceInterval90[1]}\n"
                      + $"ppp vlasy;;;{HairTeamSeizedCount.ConfidenceInterval90[0]};{HairTeamSeizedCount.ConfidenceInterval90[1]}\n"
                      + $"ppp tvar;;;{FaceTeamSeizedCount.ConfidenceInterval90[0]};{FaceTeamSeizedCount.ConfidenceInterval90[1]}\n";

        var fileName = $"sp{Config.SelectedParkingStrategy}_"
                       + $"rc{Config.ParkingLotRowsCount}_"
                       + $"rec{Config.ReceptionEmployeeCount}_"
                       + $"hec{Config.HairEmployeeCount}_"
                       + $"fec{Config.FaceEmployeeCount}_"
                       + $"im{Config.CustomerRateMultiplier}.csv";

        await File.WriteAllTextAsync(fileName, results);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    private void Init()
    {
        BossAgent = new BossAgent(SimId.BossAgent, this, null);
        EnvironsByFootAgent = new EnvironsByFootAgent(SimId.EnvironsByFootAgent, this, BossAgent);
        BeautySalonAgent = new BeautySalonAgent(SimId.BeautySalonAgent, this, BossAgent);
        EnvironsParkingLotAgent = new EnvironsParkingLotAgent(SimId.EnvironsParkingLotAgent, this, BossAgent);
        ReceptionAgent = new ReceptionAgent(SimId.ReceptionAgent, this, BeautySalonAgent);
        HairServiceAgent = new HairServiceAgent(SimId.HairServiceAgent, this, BeautySalonAgent);
        FaceServiceAgent = new FaceServiceAgent(SimId.FaceServiceAgent, this, BeautySalonAgent);
    }

    public BossAgent BossAgent { get; set; }
    public EnvironsByFootAgent EnvironsByFootAgent { get; set; }
    public BeautySalonAgent BeautySalonAgent { get; set; }
    public EnvironsParkingLotAgent EnvironsParkingLotAgent { get; set; }
    public ReceptionAgent ReceptionAgent { get; set; }
    public HairServiceAgent HairServiceAgent { get; set; }

    public FaceServiceAgent FaceServiceAgent { get; set; }

    //meta! tag="end"


    public void Dispose()
    {
        ReceptionAgent.OnSalonClose -= SampleStatsOnSalonClose;
    }
}