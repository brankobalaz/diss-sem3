using OSPABA;

namespace simulation;

public class SimId : IdList
{
		//meta! userInfo="Generated code: do not modify", tag="begin"
		public const int BossAgent = 1;
		public const int EnvironsByFootAgent = 3;
		public const int BeautySalonAgent = 4;
		public const int EnvironsParkingLotAgent = 9;
		public const int ReceptionAgent = 6;
		public const int HairServiceAgent = 7;
		public const int FaceServiceAgent = 8;
		public const int BossManager = 101;
		public const int EnvironsByFootManager = 103;
		public const int BeautySalonManager = 104;
		public const int EnvironsParkingLotManager = 109;
		public const int ReceptionManager = 106;
		public const int HairServiceManager = 107;
		public const int FaceServiceManager = 108;
		public const int PayService = 1004;
		public const int HairService = 1005;
		public const int FaceCleansingService = 1006;
		public const int MakeupService = 1007;
		public const int CustomerArrivalByFootScheduler = 1001;
		public const int Drive = 1009;
		public const int CarArrivalScheduler = 1010;
		public const int CheckInService = 1003;
		public const int CloseSalon = 1008;
		public const int Walk = 1011;
		//meta! tag="end"
}