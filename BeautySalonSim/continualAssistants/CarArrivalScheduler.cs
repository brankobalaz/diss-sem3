using System.ComponentModel;
using OSPABA;
using simulation;
using agents;
using BeautySalonSim.utils;
using entities;
using OSPRNG;
using utils;

namespace continualAssistants;

//meta! id="88"
public class CarArrivalScheduler : Scheduler
{
    private RNG<double>? _carInterArrivalTimeRng;

    public CarArrivalScheduler(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        MyAgent.AddOwnMessage(Mc.CarGenerated);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        _carInterArrivalTimeRng = new ExponentialRNG(Config.CarInterArrivalTimeMean / Config.CustomerRateMultiplier);
    }

    //meta! sender="EnvironsParkingLotAgent", id="89", type="Start"
    public void ProcessStart(MessageForm message)
    {
        var msg = new MyMessage(MySim)
        {
            Code = Mc.CarGenerated
        };
        Hold(_carInterArrivalTimeRng!.Sample(), msg);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.CarGenerated:
                ProcessCarGenerated(message);
                break;
        }
    }

    private void ProcessCarGenerated(MessageForm message)
    {
        // Plan another arrival
        var interArrivalTime = _carInterArrivalTimeRng!.Sample();
        if (MySim.CurrentTime + interArrivalTime <= Config.SalonClosingAfterStartTime)
        {
            var msgCopy = message.CreateCopy();
            Hold(interArrivalTime, msgCopy);
        }

        // Process current
        var newCustomer = new Customer(MySim);
        newCustomer.Parking = ProcedureState.Requested;

        var carType = Config.ParkingStrategies[Config.SelectedParkingStrategy].GetType();
        var newCar = (Car) Activator.CreateInstance(carType, MySim)!;

        newCar.Customer = newCustomer;
        newCustomer.Car = newCar;

        MyAgent.AllCars.Add(newCar);
        MyAgent.AllCustomersByCar.Add(newCustomer);

        var msg = (MyMessage) message;
        msg.Car = newCar;
        AssistantFinished(message);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case IdList.Start:
                ProcessStart(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new EnvironsParkingLotAgent MyAgent => (EnvironsParkingLotAgent) base.MyAgent;
}