using OSPABA;
using simulation;
using agents;
using entities;
using OSPRNG;
using utils;

namespace continualAssistants;

//meta! id="15"
public class CustomerArrivalByFootScheduler : Scheduler
{
    private RNG<double>? _customerInterArrivalTimeRng;

    public CustomerArrivalByFootScheduler(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        myAgent.AddOwnMessage(Mc.CustomerByFootGenerated);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        _customerInterArrivalTimeRng =
            new ExponentialRNG(Config.CustomerInterArrivalTimeMean / Config.CustomerRateMultiplier);
    }

    //meta! sender="EnvironsByFootAgent", id="16", type="Start"
    public void ProcessStart(MessageForm message)
    {
        var msg = new MyMessage(MySim)
        {
            Code = Mc.CustomerByFootGenerated
        };
        Hold(_customerInterArrivalTimeRng!.Sample(), msg);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.CustomerByFootGenerated:
                ProcessCustomerGenerated(message);
                break;
        }
    }

    private void ProcessCustomerGenerated(MessageForm message)
    {
        // Plan another arrival

        var interArrivalTime = _customerInterArrivalTimeRng!.Sample();
        if (MySim.CurrentTime + interArrivalTime <= Config.SalonClosingAfterStartTime)
        {
            var msgCopy = message.CreateCopy();
            Hold(interArrivalTime, msgCopy);
        }

        // Process current
        var newCustomer = new Customer(MySim)
        {
            CheckInProcedure = ProcedureState.Requested
        };

        MyAgent.AllCustomersByFoot.Add(newCustomer);

        var msg = (MyMessage) message;
        msg.Customer = newCustomer;
        AssistantFinished(message);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case IdList.Start:
                ProcessStart(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new EnvironsByFootAgent MyAgent => (EnvironsByFootAgent) base.MyAgent;
}