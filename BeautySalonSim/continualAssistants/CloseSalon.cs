using OSPABA;
using simulation;
using agents;
namespace continualAssistants
{
	//meta! id="63"
	public class CloseSalon : Scheduler
	{
		public CloseSalon(int id, Simulation mySim, CommonAgent myAgent) :
			base(id, mySim, myAgent)
		{
			MyAgent.AddOwnMessage(Mc.CloseSalonOccurred);
		}

		override public void PrepareReplication()
		{
			base.PrepareReplication();
			// Setup component for the next replication
		}

		//meta! sender="ReceptionAgent", id="64", type="Start"
		public void ProcessStart(MessageForm message)
		{
			message.Code = Mc.CloseSalonOccurred;
			Hold(Config.SalonClosingAfterStartTime, message);
		}

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
		{
			switch (message.Code)
			{
				case Mc.CloseSalonOccurred:
					AssistantFinished(message);
					break;
			}
		}

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
		public new ReceptionAgent MyAgent
		{
			get
			{
				return (ReceptionAgent)base.MyAgent;
			}
		}
	}
}