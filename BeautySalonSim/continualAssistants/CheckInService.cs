using OSPABA;
using simulation;
using agents;
using OSPRNG;
using utils;

namespace continualAssistants;

//meta! id="30"
public class CheckInService : Process
{
    private RNG<double>? _checkInServiceTimeRng;
    private RNG<double>? _proceduresSelectionRng;
    private RNG<double>? _faceCleansingSelectionRng;

    public CheckInService(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        MyAgent.AddOwnMessage(Mc.CheckInServiceDone);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication
        _checkInServiceTimeRng = new UniformContinuousRNG(Config.CheckInServiceTimeMin, Config.CheckInServiceTimeMax);
        _proceduresSelectionRng = new UniformContinuousRNG(0, 1);
        _faceCleansingSelectionRng = new UniformContinuousRNG(0, 1);
    }

		//meta! sender="ReceptionAgent", id="31", type="Start"
		public void ProcessStart(MessageForm message)
    {
        var msg = (MyMessage) message;
        var customer = msg.Customer!;

        // Determine procedures for customer
        var proceduresRand = _proceduresSelectionRng!.Sample();
        if (proceduresRand < Config.HairAndFaceProceduresRate) // Hair & face
        {
            customer.WashingProcedure = ProcedureState.Requested;
            customer.HairProcedure = ProcedureState.Requested;
            customer.MakeupProcedure = ProcedureState.Requested;
        }
        else if (proceduresRand < Config.HairAndFaceProceduresRate + Config.HairProcedureRate) // Only hair
        {
            customer.WashingProcedure = ProcedureState.Requested;
            customer.HairProcedure = ProcedureState.Requested;
        }
        else // Only makeup
        {
            customer.MakeupProcedure = ProcedureState.Requested;
        }

        if (customer.MakeupProcedure == ProcedureState.Requested)
        {
            var cleansingRand = _faceCleansingSelectionRng!.Sample();
            if (cleansingRand < Config.FaceCleansingProcedureRate)
                customer.FaceCleansingProcedure = ProcedureState.Requested;
        }

        customer.PayProcedure = ProcedureState.Requested;

        message.Code = Mc.CheckInServiceDone;
        Hold(_checkInServiceTimeRng!.Sample(), message);
    }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.CheckInServiceDone:
                AssistantFinished(message);
                break;
        }
    }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
    public new ReceptionAgent MyAgent => (ReceptionAgent) base.MyAgent;
}