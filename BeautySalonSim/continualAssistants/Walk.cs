using OSPABA;
using simulation;
using agents;
using BeautySalonSim.utils;
using entities;
using OSPRNG;
using utils;

namespace continualAssistants;

//meta! id="95"
public class Walk : Process
{
    private RNG<double>? _walkSpeedRng;

    public Walk(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        myAgent.AddOwnMessage(Mc.WalkDone);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        _walkSpeedRng = new UniformContinuousRNG(Config.WalkSpeedMin, Config.WalkSpeedMax);
    }

    //meta! sender="EnvironsParkingLotAgent", id="96", type="Start"
    public void ProcessStart(MessageForm message)
    {
        var msg = (MyMessage) message;
        var car = msg.Customer!.Car!;
        var parkPos = car.CurrentPos;

        var distance = ParkDistanceMap.GetSalonWalkDistance(parkPos);
        var duration = distance / _walkSpeedRng!.Sample();
        
        message.Code = Mc.WalkDone;
        Hold(duration, message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.WalkDone:

                var msg = (MyMessage) message;

                if (msg.Customer!.WalkToSalon == ProcedureState.InProgress)
                    msg.Customer!.WalkToSalon = ProcedureState.Done;
                else if (msg.Customer!.WalkToCar == ProcedureState.InProgress)
                    msg.Customer!.WalkToCar = ProcedureState.Done;

                AssistantFinished(message);
                break;
        }
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case IdList.Start:
                ProcessStart(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new EnvironsParkingLotAgent MyAgent => (EnvironsParkingLotAgent) base.MyAgent;
}