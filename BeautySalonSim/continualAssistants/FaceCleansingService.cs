using OSPABA;
using simulation;
using agents;
using OSPRNG;

namespace continualAssistants;

//meta! id="39"
public class FaceCleansingService : Process
{
    private RNG<double>? _faceCleansingServiceTimeRng;

    public FaceCleansingService(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        myAgent.AddOwnMessage(Mc.FaceCleansingServiceDone);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        _faceCleansingServiceTimeRng = new TriangularRNG(
            Config.FaceCleansingServiceTimeMin,
            Config.FaceCleansingServiceTimeMode,
            Config.FaceCleansingServiceTimeMax);
    }

		//meta! sender="FaceServiceAgent", id="40", type="Start"
		public void ProcessStart(MessageForm message)
    {
        message.Code = Mc.FaceCleansingServiceDone;
        Hold(_faceCleansingServiceTimeRng!.Sample(), message);
    }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.FaceCleansingServiceDone:
                AssistantFinished(message);
                break;
        }
    }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
    public new FaceServiceAgent MyAgent => (FaceServiceAgent) base.MyAgent;
}