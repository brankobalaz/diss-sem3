using OSPABA;
using simulation;
using agents;
using OSPRNG;

namespace continualAssistants;

//meta! id="32"
public class PayService : Process
{
    private RNG<double>? _payServiceTimeRng;

    public PayService(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        myAgent.AddOwnMessage(Mc.PayServiceDone);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        _payServiceTimeRng = new UniformContinuousRNG(Config.PayServiceTimeMin, Config.PayServiceTimeMax);
    }

		//meta! sender="ReceptionAgent", id="33", type="Start"
		public void ProcessStart(MessageForm message)
    {
        message.Code = Mc.PayServiceDone;
        Hold(_payServiceTimeRng!.Sample(), message);
    }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.PayServiceDone:
                AssistantFinished(message);
                break;
        }
    }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
    public new ReceptionAgent MyAgent => (ReceptionAgent) base.MyAgent;
}