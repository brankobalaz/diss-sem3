using OSPABA;
using simulation;
using agents;
using OSPRNG;
using utils;

namespace continualAssistants;

//meta! id="36"
public class HairService : Process
{
    private RNG<double>? _hairServiceTypeRng;
    private RNG<int>? _simpleHairServiceTimeRng;
    private RNG<int>? _complexHairServiceTimeRng;
    private RNG<int>? _weddingHairServiceTimeRng;

    public HairService(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        myAgent.AddOwnMessage(Mc.HairServiceDone);
        myAgent.AddOwnMessage(Mc.WashingDone);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        _hairServiceTypeRng = new UniformContinuousRNG(0, 1);

        _simpleHairServiceTimeRng = new UniformDiscreteRNG(10, 30);

        var complexAttributes = new EmpiricPair<int>[]
        {
            new(new UniformDiscreteRNG(30, 60), 0.4),
            new(new UniformDiscreteRNG(61, 120), 0.6)
        };

        _complexHairServiceTimeRng = new EmpiricRNG<int>(complexAttributes);

        var weddingAttributes = new EmpiricPair<int>[]
        {
            new(new UniformDiscreteRNG(50, 60), 0.2),
            new(new UniformDiscreteRNG(61, 100), 0.3),
            new(new UniformDiscreteRNG(101, 150), 0.5)
        };
        _weddingHairServiceTimeRng = new EmpiricRNG<int>(weddingAttributes);
    }

    //meta! sender="HairServiceAgent", id="37", type="Start"
    public void ProcessStart(MessageForm message)
    {
        var serviceDuration = Config.HairWashingTime;
        message.Code = Mc.WashingDone;
        Hold(serviceDuration, message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.WashingDone:
                ProcessWashingDone(message);
                break;
            case Mc.HairServiceDone:
                AssistantFinished(message);
                break;
        }
    }

    private void ProcessWashingDone(MessageForm message)
    {
        var msg = (MyMessage) message;
        msg.Customer!.WashingProcedure = ProcedureState.Done;
        msg.Customer!.HairProcedure = ProcedureState.InProgress;
        
        // Determine hold time
        var hairTypeRand = _hairServiceTypeRng!.Sample();

        var serviceDuration = -1.0;

        if (hairTypeRand < Config.SimpleHairProcedureRate) // Simple hair procedure
            serviceDuration = _simpleHairServiceTimeRng!.Sample() * Config.SecondsInMinute;
        else if (hairTypeRand <
                 Config.SimpleHairProcedureRate + Config.ComplexHairProcedureRate) // Complex hair procedure
            serviceDuration = _complexHairServiceTimeRng!.Sample() * Config.SecondsInMinute;
        else // Wedding hair procedure
            serviceDuration = _weddingHairServiceTimeRng!.Sample() * Config.SecondsInMinute;

        message.Code = Mc.HairServiceDone;
        Hold(serviceDuration, message);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case IdList.Start:
                ProcessStart(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new HairServiceAgent MyAgent => (HairServiceAgent) base.MyAgent;
}