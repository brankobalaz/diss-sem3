using OSPABA;
using simulation;
using agents;
using OSPRNG;

namespace continualAssistants;

//meta! id="41"
public class MakeupService : Process
{
    private RNG<double>? _makeupServiceTypeRng;
    private RNG<int>? _simpleMakeupServiceTimeRng;
    private RNG<int>? _complexMakeupServiceTimeRng;

    public MakeupService(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        myAgent.AddOwnMessage(Mc.MakeupServiceDone);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication

        _makeupServiceTypeRng = new UniformContinuousRNG(0, 1);
        _simpleMakeupServiceTimeRng = new UniformDiscreteRNG(10, 25);
        _complexMakeupServiceTimeRng = new UniformDiscreteRNG(20, 100);
    }

		//meta! sender="FaceServiceAgent", id="42", type="Start"
		public void ProcessStart(MessageForm message)
    {
        var serviceTypeRand = _makeupServiceTypeRng!.Sample();

        var serviceDuration = -1.0;
        if (serviceTypeRand < Config.ComplexMakeupProcedureRate) // Complex makeup
            serviceDuration = _complexMakeupServiceTimeRng!.Sample() * Config.SecondsInMinute;
        else // Simple makeup
            serviceDuration = _simpleMakeupServiceTimeRng!.Sample() * Config.SecondsInMinute;

        message.Code = Mc.MakeupServiceDone;
        Hold(serviceDuration, message);
    }

		//meta! userInfo="Process messages defined in code", id="0"
		public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.MakeupServiceDone:
                AssistantFinished(message);
                break;
        }
    }

		//meta! userInfo="Generated code: do not modify", tag="begin"
		override public void ProcessMessage(MessageForm message)
		{
			switch (message.Code)
			{
			case Mc.Start:
				ProcessStart(message);
			break;

			default:
				ProcessDefault(message);
			break;
			}
		}
		//meta! tag="end"
    public new FaceServiceAgent MyAgent => (FaceServiceAgent) base.MyAgent;
}