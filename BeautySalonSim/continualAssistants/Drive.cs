using OSPABA;
using simulation;
using agents;
using BeautySalonSim.utils;
using entities;
using utils;

namespace continualAssistants;

//meta! id="85"
public class Drive : Process
{
    public Drive(int id, Simulation mySim, CommonAgent myAgent) :
        base(id, mySim, myAgent)
    {
        MyAgent.AddOwnMessage(Mc.DriveDone);
    }

    override public void PrepareReplication()
    {
        base.PrepareReplication();
        // Setup component for the next replication
    }

    //meta! sender="EnvironsParkingLotAgent", id="86", type="Start"
    public void ProcessStart(MessageForm message)
    {
        var car = ((MyMessage) message).Car!;

        var duration = ParkDistanceMap.GetCarDriveTime(car.CurrentPos, car.DestinationPos);

        message.Code = Mc.DriveDone;
        Hold(duration, message);
    }

    //meta! userInfo="Process messages defined in code", id="0"
    public void ProcessDefault(MessageForm message)
    {
        switch (message.Code)
        {
            case Mc.DriveDone:
                ProcessDriveDone(message);
                break;
        }
    }

    private void ProcessDriveDone(MessageForm message)
    {
        var msg = (MyMessage) message;
        var car = msg.Car!;

        car.CurrentPos = new Pos(car.DestinationPos);

        AssistantFinished(message);
    }

    //meta! userInfo="Generated code: do not modify", tag="begin"
    override public void ProcessMessage(MessageForm message)
    {
        switch (message.Code)
        {
            case IdList.Start:
                ProcessStart(message);
                break;

            default:
                ProcessDefault(message);
                break;
        }
    }

    //meta! tag="end"
    public new EnvironsParkingLotAgent MyAgent => (EnvironsParkingLotAgent) base.MyAgent;
}