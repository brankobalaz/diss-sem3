﻿// See https://aka.ms/new-console-template for more information

using System.Globalization;
using entities;
using simulation;

var sim = new MySimulation();

Config.SelectedParkingStrategy = 2;
Config.CustomerRateMultiplier = 1;

Config.ParkingLotRowsCount = 2;
Config.ReceptionEmployeeCount = 3;
Config.HairEmployeeCount = 11;
Config.FaceEmployeeCount = 8;
sim.Simulate(8000);



return;
for (var i = 7; i <= 15; i += 1)
{
    Config.ColBias = i;
    sim.Simulate(300);
    Console.WriteLine(
        $"{i}: {sim.ParkingSatisfactionStat.ConfidenceInterval90[0]} - {sim.ParkingSatisfactionStat.ConfidenceInterval90[1]}");
}


return;

Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("sk");

var s = "item;time_1;time_2;num_1;num_2\n";
s +=
    $"čas v salone (do konca);{TimeSpan.FromSeconds(sim.TimeInSystemStat.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(sim.TimeInSystemStat.ConfidenceInterval90[1]).ToString("g")};;\n";
s +=
    $"čas v salone (do 17:00);{TimeSpan.FromSeconds(sim.TimeInSystemAtClosingStat.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(sim.TimeInSystemAtClosingStat.ConfidenceInterval90[1]).ToString("g")};;\n";
s +=
    $"chladnutie;{TimeSpan.FromSeconds(sim.CoolDownTimeStat.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(sim.CoolDownTimeStat.ConfidenceInterval90[1]).ToString("g")};;\n";
s +=
    $"čas v check-in;{TimeSpan.FromSeconds(sim.TimeInCheckInFrontAtModelEnd.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(sim.TimeInCheckInFrontAtModelEnd.ConfidenceInterval90[1]).ToString("g")};;\n";
s +=
    $"čas hair;{TimeSpan.FromSeconds(sim.TimeInHairFrontAtClosing.ConfidenceInterval90[0]).ToString("g")};{TimeSpan.FromSeconds(sim.TimeInHairFrontAtClosing.ConfidenceInterval90[1]).ToString("g")};;\n";
s +=
    $"obslúžených;;;{sim.ServedCustomersStat.ConfidenceInterval90[0]};{sim.ServedCustomersStat.ConfidenceInterval90[1]}\n";
s +=
    $"NEobslúžených;;;{sim.LeftCustomersCountStat.ConfidenceInterval90[0]};{sim.LeftCustomersCountStat.ConfidenceInterval90[1]}\n";
s +=
    $"front check in;;;{sim.CheckInFrontLengthAtClosingStat.ConfidenceInterval90[0]};{sim.CheckInFrontLengthAtClosingStat.ConfidenceInterval90[1]}\n";
s +=
    $"front hair;;;{sim.HairFrontLengthAtClosingStat.ConfidenceInterval90[0]};{sim.HairFrontLengthAtClosingStat.ConfidenceInterval90[1]}\n";
s +=
    $"front face;;;{sim.FaceFrontLengthAtClosingStat.ConfidenceInterval90[0]};{sim.FaceFrontLengthAtClosingStat.ConfidenceInterval90[1]}\n";
s +=
    $"front pay;;;{sim.PayFrontLengthAtClosingStat.ConfidenceInterval90[0]};{sim.PayFrontLengthAtClosingStat.ConfidenceInterval90[1]}\n";

await File.WriteAllTextAsync("results.txt", s);

/*
return;
// Zo spawn na križovatku
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(0, 15, 1, 0)} ?= 48");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(0, 15, 2, 0)} ?= 58");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(0, 15, 3, 0)} ?= 66");
// Na ďalšiu križovatku
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(1, 0, 2, 0)} ?= 10");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(2, 0, 3, 0)} ?= 8");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(1, 0, 3, 0)} ?= 18");
// Posun o 1 miesto v rovnakom rade
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(1, 0, 1, 1)} ?= 2.33");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(1, 5, 1, 6)} ?= 2.33");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(3, 1, 3, 2)} ?= 2.33");
// Z miesta na spawn
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(1, 15, 0, 15)} ?= 13");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(1, 14, 0, 15)} ?= 15.33");
// Z posledného miesta na prvú križovatku
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(1, 15, 1, 0)} ?= 61");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(2, 15, 1, 0)} ?= 71");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(3, 15, 1, 0)} ?= 79");
// Z miesta na iné miesto
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(2, 15, 2, 1)} ?= 83,33");
Console.WriteLine($"{ParkDistanceMap.GetCarDriveTime(2, 2, 2, 1)} ?= 113,66");
*/