﻿namespace BeautySalonGui.Utils;

public class CiGuiElement
{
    public double[] Ci { get; set; }
    public string Title { get; set; }
}