﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Threading;
using BeautySalonGui.Utils;
using BeautySalonSim.utils;
using Caliburn.Micro;
using entities;
using simulation;
using OSPABA;

namespace BeautySalonGui.ViewModels;

public class BeautySalonViewModel : Screen, ISimDelegate, IDisposable
{
    private const double SlowdownDurationSeconds = 0.1;
    private const int MinTimeBetweenGuiUpdate = 75 * 10_000; // 75 ms

    private readonly MySimulation _salonSim;
    private double _slowdownPeriodSlider;

    private int _replicationMaxCount;
    private long _currentReplication;

    private ParkingLotMap _parkingLotMap;
    private ParkingLotUtilizationMap _parkingLotUtilizationMap;

    private bool _isUiEnabled = true;
    private double _simulationTimeClock;
    private bool _isTimeVirtual;
    private long _lastGlobalStatisticsUpdateTime;
    private long _lastSimulationUpdateTime;

    private double _oneIterationReceptionEmployeesUtilization;
    private double _oneIterationHairEmployeesUtilization;
    private double _oneIterationFaceEmployeesUtilization;

    private double[] _oneIterationInModelCi;
    public bool IsUiDisabled => !IsUiEnabled;

    private CiGuiElement _simulationTimeInSalonCgi = new() {Title = "Čas zákazníka v salóne (do konca)"};
    private CiGuiElement _simulationTimeInSalonAtClosingCgi = new() {Title = "Čas zákazníka v salóne (do 17:00)"};

    private CiGuiElement _simulationTimeInCheckInFrontAtClosingCgi =
        new() {Title = "Čas zákazníka v Check-In fronte (do 17:00)"};

    private CiGuiElement _simulationParkingSuccessRateCgi = new() {Title = "Pravdepodobnosť zaparkovania"};
    private CiGuiElement _simulationParkingSatisfactionCgi = new() {Title = "Spokojnosť s parkovaním"};

    private CiGuiElement _simulationTimeInHairFrontAtClosingCgi = new() {Title = "Čas zákazníka v Hair fronte"};
    private CiGuiElement _simulationTimeInFaceFrontAtClosingCgi = new() {Title = "Čas zákazníka v Face fronte"};
    private CiGuiElement _simulationTimeInPayFrontAtClosingCgi = new() {Title = "Čas zákazníka v Pay fronte"};

    private CiGuiElement _simulationCoolingTimeCgi = new() {Title = "Čas chladnutia simulácie"};


    private CiGuiElement _simulationServedCustomersCountCgi = new() {Title = "Počet obslúžených zákazníkov"};
    private CiGuiElement _simulationLeftCustomersCountCgi = new() {Title = "Počet Neobslúžených zákazníkov"};

    private CiGuiElement _simulationCheckInFrontLengthAtClosingCgi = new() {Title = "Dĺžka Check-In frontu"};
    private CiGuiElement _simulationHairFrontLengthAtClosingCgi = new() {Title = "Dĺžka Hair frontu"};
    private CiGuiElement _simulationFaceFrontLengthAtClosingCgi = new() {Title = "Dĺžka Face frontu"};
    private CiGuiElement _simulationPayFrontLengthAtClosingCgi = new() {Title = "Dĺžka Pay frontu"};

    private CiGuiElement _simulationReceptionTeamAtClosingCgi = new() {Title = "Priem. poč. prac. recepčných"};
    private CiGuiElement _simulationHairTeamAtClosingCgi = new() {Title = "Ppp kaderníčom"};
    private CiGuiElement _simulationFaceTeamAtClosingCgi = new() {Title = "Ppp kozmetičiek"};

    private int _oneIterationServedCustomersCount;
    private double _oneIterationTimeInSystemMean;
    private double _parkingSuccessRate;
    private double _parkingSatisfaction;

    public int ReceptionistsCount { get; set; }
    public int HairdressersCount { get; set; }
    public int FaceCaresCount { get; set; }
    public double CustomerRateMultiplier { get; set; }

    public ObservableCollection<Car> ParkingStrategies { get; } = new();
    public ObservableCollection<int> ParkingRowsCountOptions { get; } = new() {1, 2, 3};

    public BindableCollection<Employee> ReceptionEmployees { get; } = new();
    public BindableCollection<Employee> HairEmployees { get; } = new();
    public BindableCollection<Employee> FaceEmployees { get; } = new();

    public BindableCollection<Customer> AllCustomersByFoot { get; } = new();
    public BindableCollection<Customer> AllCustomersByCar { get; } = new();

    public BindableCollection<MyMessage> ToFaceQueue { get; } = new();
    public BindableCollection<MyMessage> ToHairQueue { get; } = new();
    public BindableCollection<MyMessage> ToCheckInQueue { get; } = new();
    public BindableCollection<MyMessage> ToPayQueue { get; } = new();

    public BindableCollection<CiGuiElement> CiTimeStatistics { get; } = new();
    public BindableCollection<CiGuiElement> CiStatistics { get; } = new();


    public bool IsNotTimeVirtual => !IsTimeVirtual;

    public bool IsTimeVirtual
    {
        get => _isTimeVirtual;
        set
        {
            _isTimeVirtual = value;

            NotifyOfPropertyChange();
            NotifyOfPropertyChange(nameof(IsNotTimeVirtual));

            if (value)
                _salonSim.SafeSetMaxSimSpeed();
            else
                _salonSim.SafeSetSimSpeed(SlowdownPeriodSlider, SlowdownDurationSeconds);
        }
    }

    public int SelectedParkingStrategy { get; set; }
    public int SelectedParkingRowCount { get; set; }

    public ParkingLotMap ParkingLotMap
    {
        get => _parkingLotMap;
        set
        {
            _parkingLotMap = value;
            NotifyOfPropertyChange();
        }
    }

    public ParkingLotUtilizationMap ParkingLotUtilizationMap
    {
        get => _parkingLotUtilizationMap;
        set
        {
            _parkingLotUtilizationMap = value;
            NotifyOfPropertyChange();
        }
    }

    public double SlowdownPeriodSlider
    {
        get => _slowdownPeriodSlider;
        set
        {
            _slowdownPeriodSlider = value;
            _salonSim.SafeSetSimSpeed(value, SlowdownDurationSeconds);
        }
    }

    public bool IsUiEnabled
    {
        get => _isUiEnabled;
        set
        {
            _isUiEnabled = value;
            NotifyOfPropertyChange();
            NotifyOfPropertyChange(nameof(IsUiDisabled));
        }
    }

    public long CurrentReplication
    {
        get => _currentReplication;
        set
        {
            _currentReplication = value;
            NotifyOfPropertyChange();
        }
    }

    public double SimulationTimeClock
    {
        get => _simulationTimeClock;
        set
        {
            _simulationTimeClock = value;
            NotifyOfPropertyChange();
        }
    }

    public int ReplicationMaxCount
    {
        get => _replicationMaxCount;
        set
        {
            _replicationMaxCount = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationReceptionEmployeesUtilization
    {
        get => _oneIterationReceptionEmployeesUtilization;
        set
        {
            _oneIterationReceptionEmployeesUtilization = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationHairEmployeesUtilization
    {
        get => _oneIterationHairEmployeesUtilization;
        set
        {
            _oneIterationHairEmployeesUtilization = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationFaceEmployeesUtilization
    {
        get => _oneIterationFaceEmployeesUtilization;
        set
        {
            _oneIterationFaceEmployeesUtilization = value;
            NotifyOfPropertyChange();
        }
    }

    public int OneIterationServedCustomersCount
    {
        get => _oneIterationServedCustomersCount;
        set
        {
            _oneIterationServedCustomersCount = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationTimeInSystemMean
    {
        get => _oneIterationTimeInSystemMean;
        set
        {
            _oneIterationTimeInSystemMean = value;
            NotifyOfPropertyChange();
        }
    }


    public double ParkingSuccessRate
    {
        get => _parkingSuccessRate;
        set
        {
            _parkingSuccessRate = value;
            NotifyOfPropertyChange();
        }
    }


    public double ParkingSatisfaction
    {
        get => _parkingSatisfaction;
        set
        {
            _parkingSatisfaction = value;
            NotifyOfPropertyChange();
        }
    }

    public BeautySalonViewModel(MySimulation mySimulation)
    {
        _salonSim = mySimulation;
        _salonSim.RegisterDelegate(this);

        _slowdownPeriodSlider = 1.5;
        _isTimeVirtual = false;
        SelectedParkingStrategy = 2;
        SelectedParkingRowCount = 2;
        ReplicationMaxCount = 10_000;

        ReceptionistsCount = 2;
        HairdressersCount = 6;
        FaceCaresCount = 6;
        CustomerRateMultiplier = 1;

        Config.ParkingStrategies.ForEach(x => ParkingStrategies.Add(x));

        CiTimeStatistics.Add(_simulationTimeInSalonCgi);
        CiTimeStatistics.Add(_simulationTimeInSalonAtClosingCgi);
        CiTimeStatistics.Add(_simulationCoolingTimeCgi);

        CiTimeStatistics.Add(_simulationTimeInCheckInFrontAtClosingCgi);
        CiTimeStatistics.Add(_simulationTimeInHairFrontAtClosingCgi);
        CiTimeStatistics.Add(_simulationTimeInFaceFrontAtClosingCgi);
        CiTimeStatistics.Add(_simulationTimeInPayFrontAtClosingCgi);


        CiStatistics.Add(_simulationServedCustomersCountCgi);
        CiStatistics.Add(_simulationLeftCustomersCountCgi);
        CiStatistics.Add(_simulationCheckInFrontLengthAtClosingCgi);
        CiStatistics.Add(_simulationHairFrontLengthAtClosingCgi);
        CiStatistics.Add(_simulationFaceFrontLengthAtClosingCgi);
        CiStatistics.Add(_simulationPayFrontLengthAtClosingCgi);
        CiStatistics.Add(_simulationParkingSuccessRateCgi);
        CiStatistics.Add(_simulationParkingSatisfactionCgi);

        CiStatistics.Add(_simulationReceptionTeamAtClosingCgi);
        CiStatistics.Add(_simulationHairTeamAtClosingCgi);
        CiStatistics.Add(_simulationFaceTeamAtClosingCgi);
    }

    // Start/Pause/Stop and between replications 2x!
    public void SimStateChanged(Simulation sim, SimState state)
    {
        if (state == SimState.Running || state == SimState.Unpaused)
            IsUiEnabled = false;
        else if (state == SimState.Stopped || state == SimState.Paused) IsUiEnabled = true;

        var nowTicks = DateTime.Now.Ticks;
        if ((!_salonSim.IsReplicationRunning() ||
             nowTicks <= _lastGlobalStatisticsUpdateTime + MinTimeBetweenGuiUpdate) &&
            state != SimState.Stopped) return;
        _lastGlobalStatisticsUpdateTime = nowTicks;

        CurrentReplication = _salonSim.CurrentReplication + 1;

        if (_salonSim.CurrentReplication + 1 <= 2)
            return;

        try
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                // Update global stats

                _simulationTimeInSalonCgi.Ci = _salonSim.TimeInSalonStat.ConfidenceInterval90;
                _simulationTimeInSalonAtClosingCgi.Ci = _salonSim.TimeInSalonAtClosingStat.ConfidenceInterval90;
                _simulationCoolingTimeCgi.Ci = _salonSim.CoolDownTimeStat.ConfidenceInterval90;
                _simulationServedCustomersCountCgi.Ci = _salonSim.ServedCustomersStat.ConfidenceInterval90;
                _simulationLeftCustomersCountCgi.Ci = _salonSim.LeftCustomersCountStat.ConfidenceInterval90;
                _simulationParkingSuccessRateCgi.Ci = _salonSim.ParkingSuccessRateStat.ConfidenceInterval90;
                _simulationParkingSatisfactionCgi.Ci = _salonSim.ParkingSatisfactionStat.ConfidenceInterval90;

                _simulationTimeInCheckInFrontAtClosingCgi.Ci =
                    _salonSim.TimeInCheckInFrontAtClosing.ConfidenceInterval90;
                _simulationTimeInHairFrontAtClosingCgi.Ci = _salonSim.TimeInHairFrontAtClosing.ConfidenceInterval90;
                _simulationTimeInFaceFrontAtClosingCgi.Ci = _salonSim.TimeInFaceFrontAtClosing.ConfidenceInterval90;
                _simulationTimeInPayFrontAtClosingCgi.Ci = _salonSim.TimeInPayFrontAtClosing.ConfidenceInterval90;

                _simulationCheckInFrontLengthAtClosingCgi.Ci =
                    _salonSim.CheckInFrontLengthAtClosingStat.ConfidenceInterval90;
                _simulationHairFrontLengthAtClosingCgi.Ci = _salonSim.HairFrontLengthAtClosingStat.ConfidenceInterval90;
                _simulationFaceFrontLengthAtClosingCgi.Ci = _salonSim.FaceFrontLengthAtClosingStat.ConfidenceInterval90;
                _simulationPayFrontLengthAtClosingCgi.Ci = _salonSim.PayFrontLengthAtClosingStat.ConfidenceInterval90;

                _simulationReceptionTeamAtClosingCgi.Ci = _salonSim.ReceptionTeamSeizedCount.ConfidenceInterval90;
                _simulationHairTeamAtClosingCgi.Ci = _salonSim.HairTeamSeizedCount.ConfidenceInterval90;
                _simulationFaceTeamAtClosingCgi.Ci = _salonSim.FaceTeamSeizedCount.ConfidenceInterval90;

                ParkingLotUtilizationMap = _salonSim.ParkingLotUtilizationStat.Map;

                CiTimeStatistics.Refresh();
                CiStatistics.Refresh();
            }, DispatcherPriority.Background);
        }
        catch
        {
            // Is called only on exit during simulation still running
        }
    }

    // Invoked only during slow run 
    public void Refresh(Simulation sim)
    {
        var nowTicks = DateTime.Now.Ticks;
        if (nowTicks <= _lastSimulationUpdateTime + MinTimeBetweenGuiUpdate) return;
        _lastSimulationUpdateTime = nowTicks;

        try
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                SimulationTimeClock = _salonSim.CurrentTime + Config.SalonOpenClockSeconds;

                OneIterationServedCustomersCount = _salonSim.ReceptionAgent.ServedCustomersCount;
                OneIterationTimeInSystemMean = _salonSim.BeautySalonAgent.TimeInSalonStat.Mean();

                ParkingLotMap = _salonSim.EnvironsParkingLotAgent.ParkingLotMap;
                ParkingSuccessRate = _salonSim.EnvironsParkingLotAgent.ParkingSuccessRate;
                ParkingSatisfaction = _salonSim.EnvironsParkingLotAgent.ParkingSatisfaction.Mean();

                RefreshAllByFootCustomers();
                RefreshAllByCarCustomers();
                RefreshCustomerQueues();
                RefreshReceptionEmployees();
                RefreshHairEmployees();
                RefreshFaceEmployees();
            }, TimeSpan.FromMilliseconds(1), DispatcherPriority.Background);
        }
        catch
        {
            // Is called only on exit during simulation still running
        }
    }

    private void RefreshAllByCarCustomers()
    {
        AllCustomersByCar.IsNotifying = false;
        AllCustomersByCar.Clear();
        AllCustomersByCar.IsNotifying = true;
        AllCustomersByCar.AddRange(_salonSim.EnvironsParkingLotAgent.AllCustomersByCar);
    }


    public void StopSimulation()
    {
        _salonSim.StopSimulation();
    }

    public void PauseSimulation()
    {
        _salonSim.PauseSimulation();
    }

    public void StartResumeSimulation()
    {
        if (_salonSim.IsPaused())
        {
            _salonSim.ResumeSimulation();
        }
        else
        {
            Config.SelectedParkingStrategy = SelectedParkingStrategy;
            Config.ParkingLotRowsCount = ParkingRowsCountOptions[SelectedParkingRowCount];
            Config.ReceptionEmployeeCount = ReceptionistsCount;
            Config.HairEmployeeCount = HairdressersCount;
            Config.FaceEmployeeCount = FaceCaresCount;
            Config.CustomerRateMultiplier = CustomerRateMultiplier;

            if (!IsTimeVirtual)
                _salonSim.SafeSetSimSpeed(SlowdownPeriodSlider, SlowdownDurationSeconds);

            _salonSim.SimulateAsync(ReplicationMaxCount);
        }
    }


    private void RefreshCustomerQueues()
    {
        ToFaceQueue.IsNotifying = false;
        ToHairQueue.IsNotifying = false;
        ToCheckInQueue.IsNotifying = false;
        ToPayQueue.IsNotifying = false;

        ToFaceQueue.Clear();
        ToHairQueue.Clear();
        ToCheckInQueue.Clear();
        ToPayQueue.Clear();

        ToFaceQueue.IsNotifying = true;
        ToHairQueue.IsNotifying = true;
        ToCheckInQueue.IsNotifying = true;
        ToPayQueue.IsNotifying = true;

        ToFaceQueue.AddRange(_salonSim.FaceServiceAgent.FaceQueue);
        ToHairQueue.AddRange(_salonSim.HairServiceAgent.HairQueue);
        ToCheckInQueue.AddRange(_salonSim.ReceptionAgent.CheckInQueue);
        ToPayQueue.AddRange(_salonSim.ReceptionAgent.PayQueue);
    }

    private void RefreshAllByFootCustomers()
    {
        AllCustomersByFoot.IsNotifying = false;
        AllCustomersByFoot.Clear();
        AllCustomersByFoot.IsNotifying = true;
        AllCustomersByFoot.AddRange(_salonSim.EnvironsByFootAgent.AllCustomersByFoot);
    }

    private void RefreshReceptionEmployees()
    {
        ReceptionEmployees.IsNotifying = false;
        ReceptionEmployees.Clear();
        ReceptionEmployees.IsNotifying = true;
        ReceptionEmployees.AddRange(_salonSim.ReceptionAgent.EmployeeTeam.Employees);

        OneIterationReceptionEmployeesUtilization = _salonSim.ReceptionAgent.EmployeeTeam.Utilization;
    }

    private void RefreshHairEmployees()
    {
        HairEmployees.IsNotifying = false;
        HairEmployees.Clear();
        HairEmployees.IsNotifying = true;
        HairEmployees.AddRange(_salonSim.HairServiceAgent.EmployeeTeam.Employees);

        OneIterationHairEmployeesUtilization = _salonSim.HairServiceAgent.EmployeeTeam.Utilization;
    }

    private void RefreshFaceEmployees()
    {
        FaceEmployees.IsNotifying = false;
        FaceEmployees.Clear();
        FaceEmployees.IsNotifying = true;
        FaceEmployees.AddRange(_salonSim.FaceServiceAgent.EmployeeTeam.Employees);

        OneIterationFaceEmployeesUtilization = _salonSim.FaceServiceAgent.EmployeeTeam.Utilization;
    }

    public void Dispose()
    {
        _salonSim.StopSimulation();
        _salonSim.RemoveDelegate(this);
    }

    protected override Task OnDeactivateAsync(bool close, CancellationToken cancellationToken)
    {
        _salonSim.StopSimulation();
        return Task.Run(() => { _salonSim.RemoveDelegate(this); }, cancellationToken);
    }
}