﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using entities;
using OSPABA;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using simulation;

namespace BeautySalonGui.ViewModels;

public class HairdressersExperimentViewModel : Screen, ISimDelegate, IDisposable
{
    private const int MinTimeBetweenGuiUpdate = 70 * 10_000; // 70 ms

    private readonly MySimulation _salonSim;
    public PlotModel ExperimentGraph { get; } = new();
    private readonly ScatterErrorSeries _graphValues = new() {ErrorBarColor = OxyColors.Blue};

    private int _currentHairdressers;
    private double _lastGuiUpdateTime = 0;
    private bool _isUiEnabled = true;
    private bool _shouldBeStopped = false;

    public int SelectedParkingStrategy { get; set; }
    public int SelectedParkingRowCount { get; set; }

    public ObservableCollection<Car> ParkingStrategies { get; } = new();
    public ObservableCollection<int> ParkingRowsCountOptions { get; } = new() {1, 2, 3};


    public bool IsUiDisabled => !IsUiEnabled;

    public bool IsUiEnabled
    {
        get => _isUiEnabled;
        set
        {
            _isUiEnabled = value;
            NotifyOfPropertyChange();
            NotifyOfPropertyChange(nameof(IsUiDisabled));
        }
    }

    public int ReplicationsCount { get; set; }

    public int FaceCaresCount { get; set; }

    public int ReceptionistsCount { get; set; }

    public int HairdressersMaxCount { get; set; }

    public int HairdressersMinCount { get; set; }

    public double CustomerRateMultiplier { get; set; }

    public HairdressersExperimentViewModel(MySimulation salonSim)
    {
        _salonSim = salonSim;
        _salonSim.RegisterDelegate(this);

        ExperimentGraph.Title = "Závislosť dĺžky fronty na objednanie od počtu kaderníčok";
        ExperimentGraph.Axes.Add(new LinearAxis()
            {Position = AxisPosition.Bottom, Title = "Počet kaderníčok", MinimumMinorStep = 1, MinimumMajorStep = 1});
        ExperimentGraph.Axes.Add(new LinearAxis()
        {
            Position = AxisPosition.Left, Title = "Dĺžka frontu na recepcií", MinimumPadding = 0.3,
            MaximumPadding = 0.3
        });
        ExperimentGraph.Series.Add(_graphValues);

        ExperimentGraph.InvalidatePlot(true);

        ReplicationsCount = 100;
        SelectedParkingStrategy = 2;
        SelectedParkingRowCount = 1;
        FaceCaresCount = 6;
        ReceptionistsCount = 2;
        HairdressersMinCount = 1;
        HairdressersMaxCount = 10;
        CustomerRateMultiplier = 1;

        Config.ParkingStrategies.ForEach(x => ParkingStrategies.Add(x));
    }

    private bool CanRefreshSimulationGui(Simulation sim, SimState state)
    {
        if (state == SimState.Running || state == SimState.Unpaused)
            IsUiEnabled = false;
        else if (state == SimState.Stopped || state == SimState.Paused) IsUiEnabled = true;

        var nowTicks = DateTime.Now.Ticks;
        if ((!_salonSim.IsReplicationRunning() ||
             nowTicks <= _lastGuiUpdateTime + MinTimeBetweenGuiUpdate) &&
            state != SimState.Stopped) return false;
        _lastGuiUpdateTime = nowTicks;

        if (_salonSim.CurrentReplication + 1 <= 2)
            return false;

        return true;
    }

    public void SimStateChanged(Simulation sim, SimState state)
    {
        if (!CanRefreshSimulationGui(sim, state)) return;

        var mean = _salonSim.CheckInFrontLengthAtClosingStat.Mean();
        var error = _salonSim.CheckInFrontLengthAtClosingStat.Stdev();

        //var mean = _salonSim.TimeInCheckInFrontAtClosing.Mean();
        //var error = _salonSim.TimeInCheckInFrontAtClosing.Stdev();

        var newPoint = new ScatterErrorPoint(_currentHairdressers, mean, 0, error);

        var index = _currentHairdressers - HairdressersMinCount;

        if (_graphValues.Points.Count == index + 1)
            _graphValues.Points[index] = newPoint;
        else
            _graphValues.Points.Add(newPoint);

        ExperimentGraph.InvalidatePlot(true);
    }

    public void Refresh(Simulation sim)
    {
    }

    public void StopSimulation()
    {
        _salonSim.StopSimulation();
        _shouldBeStopped = true;
    }

    public void PauseSimulation()
    {
        _salonSim.PauseSimulation();
    }

    public async void StartResumeSimulation()
    {
        _shouldBeStopped = false;

        if (_salonSim.IsPaused())
        {
            _salonSim.ResumeSimulation();
        }
        else
        {
            // Clear GUI
            _graphValues.Points.Clear();
            ExperimentGraph.ResetAllAxes();
            ExperimentGraph.InvalidatePlot(true);

            IsUiEnabled = false;
            // Start experiment
            for (_currentHairdressers = HairdressersMinCount;
                 _currentHairdressers <= HairdressersMaxCount;
                 _currentHairdressers++)
            {
                Config.SelectedParkingStrategy = SelectedParkingStrategy;
                Config.ParkingLotRowsCount = ParkingRowsCountOptions[SelectedParkingRowCount];
                Config.ReceptionEmployeeCount = ReceptionistsCount;
                Config.HairEmployeeCount = _currentHairdressers;
                Config.FaceEmployeeCount = FaceCaresCount;
                Config.CustomerRateMultiplier = CustomerRateMultiplier;

                await Task.Run(() => { _salonSim.Simulate(ReplicationsCount); });

                if (_shouldBeStopped)
                    break;
            }

            IsUiEnabled = true;
        }
    }


    public void Dispose()
    {
        _salonSim.StopSimulation();
        _salonSim.RemoveDelegate(this);
    }

    protected override Task OnDeactivateAsync(bool close, CancellationToken cancellationToken)
    {
        _salonSim.StopSimulation();
        _shouldBeStopped = true;

        return Task.Run(() => { _salonSim.RemoveDelegate(this); }, cancellationToken);
    }
}