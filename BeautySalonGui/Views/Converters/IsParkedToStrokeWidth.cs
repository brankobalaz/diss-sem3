﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BeautySalonGui.Views.Converters;

public class IsParkedToStrokeWidth : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var isParked = (bool) value;

        return isParked ? 3.0 : 0.5;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}