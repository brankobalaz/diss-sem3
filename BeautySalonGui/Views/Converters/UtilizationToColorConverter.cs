﻿using System;
using System.Globalization;
using System.IO.Ports;
using System.Windows.Data;
using System.Windows.Media;

namespace BeautySalonGui.Views.Converters;

public class UtilizationToColorConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var util = (double) value;
        var a = (byte) (255 - util * 255);
        var color = Color.FromRgb(255, a, a);

        return color.ToString();
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}