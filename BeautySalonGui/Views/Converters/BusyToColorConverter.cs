﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BeautySalonGui.Views.Converters;

public class BusyToColorConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var state = (bool) value;

        switch (state)
        {
            case false:
                return "Green";
            case true:
                return "Red";
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}